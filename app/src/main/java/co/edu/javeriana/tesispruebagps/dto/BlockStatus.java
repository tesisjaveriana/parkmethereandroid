package co.edu.javeriana.tesispruebagps.dto;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

/**
 * Bloque de un horario de reserva de un parqueadero.
 * Created by Santiago on 27/10/2015.
 */
public class BlockStatus {
    /**
     * Hora de inicio del bloque.
     */
    private Date startTime;
    /**
     * Hora de finalización del bloque.
     */
    private Date finishTime;
    /**
     * Cupos disponibles del horario.
     */
    private int avaliableReservableSlots;
    /**
     * Orden en el cuál inicia el bloque en el día.
     */
    private int ordenDia;

    /**
     *
     * @param startTime Hora de inicio del bloque.
     * @param finishTime Hora de finalización del bloque.
     * @param avaliableReservableSlots Número de cupos disponibles para reservar.
     * @param ordenDia Orden en el día de ocurrencia.
     */
    public BlockStatus(Date startTime, Date finishTime,
                       int avaliableReservableSlots, int ordenDia) {
        this.startTime = startTime;
        this.finishTime = finishTime;
        this.avaliableReservableSlots = avaliableReservableSlots;
        this.ordenDia = ordenDia;
    }

    /**
     *
     * @return Hora de inicio de la reserva.
     */
    public Date getStartTime() {
        return startTime;
    }

    /**
     *
     * @return Hora de finalización de la reserva.
     */
    public Date getFinishTime() {
        return finishTime;
    }

    /**
     *
     * @return Oden de ocurrencia en el día.
     */
    public int getOrdenDia() {
        return ordenDia;
    }

    /**
     *
     * @return Número de cupos de reserva disponibles.
     */
    public int getAvaliableReservableSlots() {
        return avaliableReservableSlots;
    }

    /**
     *
     * @return Retorna la hora de inicio en formato HH:mm.
     */
    public String getStartTimeStringFormat() {
        return new SimpleDateFormat("HH:mm", Locale.getDefault()).format(startTime);
    }

    /**
     *
     * @return Retorna la hora de finalización en formatoHH:mm.
     */
    public String getFinishTimeStringFormat() {
        return new SimpleDateFormat("HH:mm", Locale.getDefault()).format(finishTime);
    }
}
