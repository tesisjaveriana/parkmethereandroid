package co.edu.javeriana.tesispruebagps.customviews;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import co.edu.javeriana.tesispruebagps.R;

/**
 * Created by Santiago on 19/11/2015.
 */
public class HelpListAdapter extends BaseAdapter {
    private String [] result;
    private Context context;
    private int [] imageId;
    private static LayoutInflater inflater = null;

    public HelpListAdapter(Context context, String[] prgmNameList, int[] prgmImages) {
        result = prgmNameList;
        this.context = context;
        imageId = prgmImages;
        inflater = ( LayoutInflater )context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public int getCount() {
        return result.length;
    }

    @Override
    public Object getItem(int position) {
        return position;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    public class Holder
    {
        TextView tv;
        ImageView img;
    }

    @Override
    public View getView(int position, View view, ViewGroup viewGroup) {
        Holder holder=new Holder();
        View rowView;
        rowView = inflater.inflate(R.layout.help_list_layout, null);
        holder.tv=(TextView) rowView.findViewById(R.id.helpListText);
        holder.img=(ImageView) rowView.findViewById(R.id.helpListImage);
        holder.tv.setText(result[position]);
        holder.tv.setTextColor(context.getResources().getColor(R.color.app_text_color));
        holder.img.setImageResource(imageId[position]);
        return rowView;
    }
}
