package co.edu.javeriana.tesispruebagps.gui;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Typeface;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.parse.ParseException;

import co.edu.javeriana.tesispruebagps.R;
import co.edu.javeriana.tesispruebagps.account.AccountController;
import co.edu.javeriana.tesispruebagps.universal.UniversalPersistence;
import co.edu.javeriana.tesispruebagps.universal.UniversalTestValues;

public class LoginActivity extends AppCompatActivity {
    public static Activity toFinish;
    private AccountController accountController;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        UniversalPersistence.setSharedPreferences(getApplicationContext());
        accountController = new AccountController();
        if(accountController.getUserID() != null) {
            Bundle extras;
            Intent signupIntent;

            extras = new Bundle();
            extras.putSerializable("account", accountController);
            signupIntent = new Intent(LoginActivity.this, MapsActivity.class);
            signupIntent.putExtras(extras);
            startActivity(signupIntent);
            finish();
        }

        toFinish = this;
        Button loginButton = (Button) findViewById(R.id.loginLoginButton);
        Button signupButton = (Button) findViewById(R.id.loginSignupButton);

        loginButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                EditText usernameView = (EditText) findViewById(R.id.loginUsername);
                EditText passwordView = (EditText) findViewById(R.id.loginPassword);

                String username = usernameView.getText().toString();
                String password = passwordView.getText().toString();

                try {
                    accountController.login(username, password);

                    Bundle extras;
                    Intent mainIntent;

                    extras = new Bundle();
                    extras.putSerializable("account", accountController);
                    mainIntent = new Intent(LoginActivity.this, MapsActivity.class);
                    mainIntent.putExtras(extras);
                    startActivity(mainIntent);
                    finish();
                } catch (ParseException e) {
                    switch ((e.getCode())) {
                        case ParseException.ACCOUNT_ALREADY_LINKED:
                            Toast.makeText(LoginActivity.this, "Ya existe la cuenta", Toast.LENGTH_SHORT).show();
                            break;
                        case ParseException.CONNECTION_FAILED:
                            Toast.makeText(LoginActivity.this, "Fallo la conexión", Toast.LENGTH_SHORT).show();
                            break;
                        case ParseException.EMAIL_MISSING:
                            Toast.makeText(LoginActivity.this, "No s eingreso el correo", Toast.LENGTH_SHORT).show();
                            break;
                        case ParseException.EMAIL_TAKEN:
                            Toast.makeText(LoginActivity.this, "Ya existe el correo", Toast.LENGTH_SHORT).show();
                            break;
                        case ParseException.PASSWORD_MISSING:
                            Toast.makeText(LoginActivity.this, "Contraseña incorrecta", Toast.LENGTH_SHORT).show();
                            break;
                        case ParseException.USERNAME_TAKEN:
                            Toast.makeText(LoginActivity.this, "Ya existe el nombre de usuario", Toast.LENGTH_SHORT).show();
                            break;
                        case ParseException.USERNAME_MISSING:
                            Toast.makeText(LoginActivity.this, "Nombre de usuario inexistente", Toast.LENGTH_SHORT).show();
                            break;
                        default:
                            Toast.makeText(LoginActivity.this, "Ocurrio un error, por favor intentelo nuevamente", Toast.LENGTH_SHORT).show();
                    }
                    UniversalTestValues.debug(getClass(), e.toString());
                }
            }
        });

        signupButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Bundle extras;
                Intent signupIntent;

                extras = new Bundle();
                extras.putSerializable("account", accountController);
                signupIntent = new Intent(LoginActivity.this, SignupActivity.class);
                signupIntent.putExtras(extras);
                startActivity(signupIntent);
            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_login, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void finish() {
        super.finish();
        toFinish = null;
    }
}
