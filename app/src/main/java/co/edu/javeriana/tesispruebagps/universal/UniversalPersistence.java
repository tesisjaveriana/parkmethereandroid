package co.edu.javeriana.tesispruebagps.universal;

import android.content.Context;
import android.content.SharedPreferences;

import java.util.Calendar;

/**
 * Gestiona el SharedPreference de la aplicación.
 * Created by Santiago Medina on 27/08/2015.
 */
public class UniversalPersistence
{
    private static final String preferencesName = "ParkAppPreferences";
    private static final String lastLatPreferenceName = "LastLat";
    private static final String lastLonPreferenceName = "LastLon";
    private static final String stayIDName = "StayID";
    private static final String userIDName = "UserID";
    private static final String sharingStateName = "SharingState";
    private static final String sharingLastParkingName = "LastParking";
    private static final String balanceMonitoringName = "BalanceMonitoring";
    private static final String monitoringArraiveName = "ArraiveMonitoring";
    private static final String monitoringPriceName = "PriceMonitoring";
    private static SharedPreferences appSettings;
    private static SharedPreferences.Editor appSettingsEditor;

    public static float getLastLatitude() {
        return appSettings.getFloat(lastLatPreferenceName, 0.0f);
    }

    public static float getLastLongitude() {
        return appSettings.getFloat(lastLonPreferenceName, 0.0f);
    }

    public static String getStayID() {
        return appSettings.getString(stayIDName, null);
    }

    public static String getUserID() {
        return appSettings.getString(userIDName, null);
    }

    public static boolean getSharingState() {
        return appSettings.getBoolean(sharingStateName, false);
    }

    public static String getLastParking() {
        return appSettings.getString(sharingLastParkingName, null);
    }

    public static boolean isBalanceMonitoring() {
        return appSettings.getBoolean(balanceMonitoringName, false);
    }

    public static Calendar getMonitoringArraive() {
        long time = appSettings.getLong(monitoringArraiveName, 0);
        if (time == 0) {
            return null;
        }
        Calendar arraive = Calendar.getInstance();
        arraive.setTimeInMillis(time);
        return arraive;
    }

    public static float getMonitoringPrice() {
        return appSettings.getFloat(monitoringPriceName, 0);
    }

    public static void setSharedPreferences( Context appContext ) {
        appSettings = appContext.getSharedPreferences(preferencesName, Context.MODE_PRIVATE);
        appSettingsEditor = appSettings.edit();
    }

    public static void setLastPosition( float latitude, float longitude ) {
        appSettingsEditor.putFloat( lastLatPreferenceName, latitude );
        appSettingsEditor.putFloat(lastLonPreferenceName, longitude);
        commitChanges();
    }
    public static void setStayID(String ID) {
        appSettingsEditor.putString(stayIDName, ID);
        commitChanges();
    }

    public static void setUserID(String userID) {
        appSettingsEditor.putString(userIDName, userID);
        commitChanges();
    }

    public static void setSharingState(boolean state) {
        appSettingsEditor.putBoolean(sharingStateName, state);
        commitChanges();
    }

    public static void setLastParking(String lastParkingID) {
        appSettingsEditor.putString(sharingLastParkingName, lastParkingID);
        commitChanges();
    }

    public static void setBalanceMonitoring(boolean isMonitring) {
        appSettingsEditor.putBoolean(balanceMonitoringName, isMonitring);
        commitChanges();
    }

    public static void setMonitoringArraive(Calendar arraive) {
        if(arraive == null) {
            appSettingsEditor.putLong(monitoringArraiveName, 0);
        } else {
            appSettingsEditor.putLong(monitoringArraiveName, arraive.getTimeInMillis());
        }
        commitChanges();
    }

    public static void setMonitoringPrice(float price) {
        appSettingsEditor.putFloat(monitoringPriceName, price);
        commitChanges();
    }

    private static void commitChanges() {
        appSettingsEditor.commit();
    }
}
