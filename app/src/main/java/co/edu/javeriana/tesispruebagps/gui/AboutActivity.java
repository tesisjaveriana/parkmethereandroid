package co.edu.javeriana.tesispruebagps.gui;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import co.edu.javeriana.tesispruebagps.R;

public class AboutActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_about);
    }
}
