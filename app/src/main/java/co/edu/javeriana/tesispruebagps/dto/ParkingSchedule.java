package co.edu.javeriana.tesispruebagps.dto;

import android.util.Pair;

import com.parse.ParseCloud;
import com.parse.ParseException;
import com.parse.ParseObject;
import com.parse.ParseQuery;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Horario de reserva de un parqueadero.
 * Created by Santiago on 27/10/2015.
 */
public class ParkingSchedule {
    /**
     * Parqueadero seleccionado.
     */
    private ParkingInfo selectedParking;
    /**
     * Horario del parqueadero.
     */
    private List<BlockStatus> schedule;
    /**
     * Bloque de inicio seleccionado.
     */
    private BlockStatus arriveBlock;
    /**
     * Bloque de finalización seleccionado.
     */
    private BlockStatus departureBlock;

    /**
     * Consulta el horario del parqueadero seleccionado.
     * @param selectedParking Parqueadero seleccionado.
     * @throws ParseException Lanza una excepción cuando hay un fallo en una consulta a Parse.
     */
    public ParkingSchedule(ParkingInfo selectedParking) throws ParseException {
        ParseQuery<ParseObject> parkingSchedule = ParseQuery.getQuery("HorarioReserva");
        List<ParseObject> queryResults;

        Date startTime;
        Date finishTime;
        int avaliableReservableSlots;
        int ordenDia;
        BlockStatus newBlock;

        // Consulta el horario del parqueadero.
        parkingSchedule.whereEqualTo("parqueadero", selectedParking.getParseForm());
        parkingSchedule.orderByAscending("ordenDia");
        queryResults = parkingSchedule.find();
        schedule = new ArrayList<>();

        for (ParseObject parseSchedule : queryResults) {
            startTime = parseSchedule.getDate("horaInicio");
            finishTime = parseSchedule.getDate("horaFin");
            avaliableReservableSlots = parseSchedule.getInt("cuposDisponibles");
            ordenDia = parseSchedule.getInt("ordenDia");
            newBlock = new BlockStatus(startTime, finishTime, avaliableReservableSlots, ordenDia);
            schedule.add(newBlock);
        }
        this.selectedParking = selectedParking;
    }

    /**
     *
     * @return Retorna el horario del parqueadero en una lista de pares donde el primer miembro es
     * la hora de inicio y el segundo es la hora de finalización de un bloque.
     */
    public List<Pair<String, String>> getAvaliableSchedule() {
        String startTimeString;
        String finishTimeString;
        Pair<String, String> avaliableSlot;
        Calendar currentTime = Calendar.getInstance();
        Date startDate;
        Calendar startCalendar = Calendar.getInstance();
        Calendar startTime;

        startCalendar.set(Calendar.SECOND, 0);
        List<Pair<String, String>> stringSchedule = new ArrayList<>();
        for (BlockStatus block : schedule) {
            startDate = block.getStartTime();
            startTime = Calendar.getInstance();
            startTime.setTime(startDate);
            startCalendar.set(Calendar.HOUR_OF_DAY, startTime.get(Calendar.HOUR_OF_DAY));
            startCalendar.set(Calendar.MINUTE, startTime.get(Calendar.MINUTE));
            if (block.getAvaliableReservableSlots() > 0 && currentTime.before(startCalendar)) {
                startTimeString = block.getStartTimeStringFormat();
                finishTimeString = block.getFinishTimeStringFormat();
                avaliableSlot = new Pair<>(startTimeString, finishTimeString);
                stringSchedule.add(avaliableSlot);
            }
        }
        return stringSchedule;
    }

    /**
     * Dada una franja del horario seleccionada, retorna el precio que se pagaría por esa estadía.
     * @param arraiveTime Hora de llegada.
     * @param departureTime Hora de salida.
     * @return El precia por cual se pagaría.
     */
    public double simulatePrice(String arraiveTime, String departureTime) {
        int i;
        double price = 0;
        Date arraiveDate;
        Date departureDate;
        long diff;
        long minutes;

        for (i = 0; i < schedule.size(); ++i) {
            if (schedule.get(i).getStartTimeStringFormat().equals(arraiveTime)) {
                arraiveDate = schedule.get(i).getStartTime();
                for (; i < schedule.size(); ++i) {
                    if (schedule.get(i).getFinishTimeStringFormat().equals(departureTime)) {
                        departureDate = schedule.get(i).getFinishTime();
                        diff = departureDate.getTime() - arraiveDate.getTime();
                        minutes = diff / (60 * 1000);
                        price = selectedParking.getPrice() * minutes;
                        break;
                    }
                }
                break;
            }
        }
        return price;
    }

    /**
     *
     * @return Parqueadero seleccionado.
     */
    public ParkingInfo getSelectedParking() {
        return selectedParking;
    }

    public List<Date> getDepartureList() throws ParseException {
        return selectedParking.getDepartureList();
    }

    /**
     * Verifica que la hora de llegada seleccionada este antes que la hora de salida y haya
     * disponibilidad entre las dos horas.
     * @param enterTime Hora de llegada seleccionada.
     * @param exitTime Hora de salida seleccionada.
     * @return Retorna true si la franja seleccionada es válida o false si no.
     */
    private boolean preLocalValidation(String enterTime, String exitTime) {
        int i;

        for (i = 0; i < schedule.size(); ++i) {
            if (schedule.get(i).getStartTimeStringFormat().equals(enterTime)) {
                arriveBlock = schedule.get(i);
                for (; i < schedule.size(); ++i) {
                    if (schedule.get(i).getAvaliableReservableSlots() <= 0) {
                        return false;
                    }
                    if (schedule.get(i).getFinishTimeStringFormat().equals(exitTime)) {
                        departureBlock = schedule.get(i);
                        return true;
                    }
                }
                break;
            }
        }
        return false;
    }

    /**
     * Verifica que la franja de reserva seleccionada sea válida y hace la reserva.
     * @param enterTime Hora de llegada.
     * @param exitTime Hora de salida.
     * @param userID ID del usuario.
     * @return Retorna true si la franja seleccionada es válida y hace la reserva y false en caso
     * contrario.
     * @throws ParseException Lanza una excepción cuando hay un fallo en una consulta a Parse.
     */
    public boolean validateReservation(String enterTime, String exitTime, String userID) throws ParseException {
        if (!preLocalValidation(enterTime, exitTime)) {
            return false;
        }

        Map<String,Object> params = new HashMap<>();

        params.put("parkingID", selectedParking.getObjectID());
        params.put("arriveTimeOrden", arriveBlock.getOrdenDia());
        params.put("depatureTimeOrden", departureBlock.getOrdenDia());
        params.put("userID", userID);

        return ParseCloud.callFunction("makeReservation", params);
    }
}
