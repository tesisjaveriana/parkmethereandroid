package co.edu.javeriana.tesispruebagps.parkingsearcher;

import com.google.android.gms.maps.model.LatLng;
import com.parse.FindCallback;
import com.parse.GetCallback;
import com.parse.ParseException;
import com.parse.ParseGeoPoint;
import com.parse.ParseObject;
import com.parse.ParseQuery;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ExecutionException;

import co.edu.javeriana.tesispruebagps.directions.GetRequestTask;
import co.edu.javeriana.tesispruebagps.dto.ParkingInfo;
import co.edu.javeriana.tesispruebagps.universal.UniversalTestValues;

/**
 * Gestiona la búsqueda de parqueaderos.
 * Created by Santiago on 31/10/2015.
 */
public class ParkingSearcherController {
    /**
     * Observadores suscritos a quienes se les notifica los resultados de la búsquedas.
     */
    private List<ParkingSearcherObserver> observers;

    public ParkingSearcherController() {
        this.observers = new ArrayList<>();
    }

    /**
     * Notifica a los observadores que ha ocurrido un error en la operación solicitada.
     * @param e Excepción ocurrida
     */
    private void notifyObserversException(Exception e) {
        for( ParkingSearcherObserver observer : observers ) {
            observer.notifyException(e);
        }
    }

    /**
     * Notifica a los observadores los límites y la ruta del parqueadero seleccionado.
     * @param parkingLimits Los límites del parqueadero.
     * @param route Ruta al parqueadero.
     */
    private void notifyObserversParkingLimitsAndRoute(List<LatLng> parkingLimits, List<LatLng> route) {
        for( ParkingSearcherObserver observer : observers ) {
            observer.notifyParkingLimitsAndRoute(parkingLimits, route);
        }
    }

    /**
     * Notifica a los observadores los parqueaderos cercanos encontrados.
     * @param nearParking Lista de parqueaderos cercanos.
     */
    private void notifyObserversNearParkingTolerance(List<ParkingInfo> nearParking) {
        for( ParkingSearcherObserver observer : observers ) {
            observer.notifyNearParkingTolerance(nearParking);
        }
    }

    private void notifyObserversNearParking(List<ParkingInfo> nearParking) {
        for (ParkingSearcherObserver observer : observers) {
            observer.notifyNearParking(nearParking);
        }
    }

    /**
     * Notifica a los observadores los parqueaderos visibles.
     * @param visibleParking Lista de parqueaderos visibles.
     */
    private void notifyObserversVisibleParking(List<ParkingInfo> visibleParking) {
        for( ParkingSearcherObserver observer : observers ) {
            observer.notifyVisibleParking(visibleParking);
        }
    }

    /**
     * Agrega un nuevo observador.
     * @param newObserver Un nuevo observador.
     */
    public void addObserver( ParkingSearcherObserver newObserver ) {
        observers.add(newObserver);
    }

    /**
     * Consulta los límites y ruta al parqueadero seleccionado y notifica a los observadores.
     * @param parkingName Nombre del parqueadero seleccionado.
     * @param currentPosition Posición actual.
     */
    public void lookUpParkingLimitsAndRoute(String parkingName, final LatLng currentPosition) {
        ParseQuery<ParseObject> query = ParseQuery.getQuery("Parqueadero");

        // Busca el parqueadero.
        query.whereEqualTo("nombre", parkingName);
        query.getFirstInBackground(new GetCallback<ParseObject>() {
            @Override
            public void done(ParseObject parqueadero, ParseException e) {
                if (e == null) {
                    // Crea la petición a Google Directions API.
                    ParseGeoPoint centroid = parqueadero.getParseGeoPoint("centroide");
                    List<List<Double>> badParkingCoordinates = parqueadero.getList("coordenadas");
                    List<LatLng> parkingCoordinates = new ArrayList<>();

                    for (List<Double> coordinate : badParkingCoordinates) {
                        parkingCoordinates.add(new LatLng(coordinate.get(0), coordinate.get(1)));
                    }
                    GetRequestTask directions = (GetRequestTask) new GetRequestTask().execute("https://maps.googleapis.com/maps/api/directions/json?" +
                            "origin=" + currentPosition.latitude + "," + currentPosition.longitude +
                            "&destination=" + centroid.getLatitude() + "," + centroid.getLongitude());
                    // Procesa la respuesta.
                    try {
                        JSONObject response = new JSONObject(directions.get());
                        JSONObject legs0 = response.getJSONArray("routes").getJSONObject(0).getJSONArray("legs").getJSONObject(0);
                        JSONArray steps = legs0.getJSONArray("steps");
                        double initLat;
                        double initLon;
                        double endLat;
                        double endLon;
                        List<LatLng> route = new ArrayList<>();

                        for (int i = 0; i < steps.length(); ++i) {
                            initLat = steps.getJSONObject(i).getJSONObject("start_location").getDouble("lat");
                            initLon = steps.getJSONObject(i).getJSONObject("start_location").getDouble("lng");
                            endLat = steps.getJSONObject(i).getJSONObject("end_location").getDouble("lat");
                            endLon = steps.getJSONObject(i).getJSONObject("end_location").getDouble("lng");
                            route.add(new LatLng(initLat, initLon));
                            route.add(new LatLng(endLat, endLon));
                        }
                        notifyObserversParkingLimitsAndRoute(parkingCoordinates, route);
                    } catch (InterruptedException | ExecutionException | JSONException e1) {
                        notifyObserversException(e1);
                    }

                } else {
                    notifyObserversException(e);
                }
            }
        });
    }

    /**
     * Consulta los parqueaderos cercanos de acuerdo a una posición y un radio y notifica a los
     * observadores los resultados.
     * @param currentPosition Posición desde la cual se realiza la búsqueda.
     * @param tolerance Radio donde se desea que hayan parqueaderos.
     */
    public void lookUpNearParkingTolerance(LatLng currentPosition, double tolerance) {

        ParseGeoPoint parsePosition = new ParseGeoPoint(currentPosition.latitude, currentPosition.longitude);
        ParseQuery<ParseObject> nearParkingQuery = ParseQuery.getQuery("Parqueadero");
        nearParkingQuery.whereWithinMiles("centroide", parsePosition, (tolerance / 1000.0));
        nearParkingQuery.findInBackground(new FindCallback<ParseObject>() {
            public void done(List<ParseObject> parseResults, ParseException e) {
                if (e == null) {
                    List<ParkingInfo> results = new ArrayList<>();
                    for (ParseObject parseParking : parseResults) {
                        results.add(new ParkingInfo(parseParking));
                    }
                    notifyObserversNearParkingTolerance(results);
                } else {
                    notifyObserversException(e);
                }
            }
        });
    }

    public void lookUpNearParking(LatLng currentPosition) {
        ParseGeoPoint parsePosition = new ParseGeoPoint(currentPosition.latitude, currentPosition.longitude);
        ParseQuery<ParseObject> nearParkingQuery = ParseQuery.getQuery("Parqueadero");
        nearParkingQuery.whereNear("centroide", parsePosition);
        nearParkingQuery.setLimit(20);
        nearParkingQuery.findInBackground(new FindCallback<ParseObject>() {
            @Override
            public void done(List<ParseObject> parseResults, ParseException e) {
                if (e == null) {
                    List<ParkingInfo> results = new ArrayList<>();
                    for (ParseObject parseParking : parseResults) {
                        results.add(new ParkingInfo(parseParking));
                    }
                    notifyObserversNearParking(results);
                } else {
                    notifyObserversException(e);
                }
            }
        });
    }

    /**
     * Consultas los parqueaderos visibles de acuerdo a una región.
     * @param NE Punto superior derecho de la región.
     * @param SW Punto inferior izquierdo de la región.
     */
    public void lookUpVisibleParking(LatLng NE, LatLng SW) {
        ParseGeoPoint ParseNE = new ParseGeoPoint(NE.latitude, NE.longitude);
        ParseGeoPoint ParseSW = new ParseGeoPoint(SW.latitude, SW.longitude);
        ParseQuery<ParseObject> visiblePakingQuery = ParseQuery.getQuery("Parqueadero");
        visiblePakingQuery.whereWithinGeoBox("centroide", ParseSW, ParseNE);

        visiblePakingQuery.findInBackground(new FindCallback<ParseObject>() {
            public void done(List<ParseObject> parseResults, ParseException e) {
                if (e == null) {
                    List<ParkingInfo> results = new ArrayList<>();
                    for (ParseObject parseParking : parseResults) {
                        results.add(new ParkingInfo(parseParking));
                    }
                    notifyObserversVisibleParking(results);
                } else {
                    UniversalTestValues.debug(getClass(), e.toString());
                }
            }
        });
    }
}

