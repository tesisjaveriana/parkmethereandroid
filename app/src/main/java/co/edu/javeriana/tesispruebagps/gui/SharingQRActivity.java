package co.edu.javeriana.tesispruebagps.gui;

import android.graphics.Bitmap;
import android.graphics.Color;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.ImageView;

import com.google.zxing.BarcodeFormat;
import com.google.zxing.WriterException;
import com.google.zxing.common.BitMatrix;
import com.google.zxing.qrcode.QRCodeWriter;

import co.edu.javeriana.tesispruebagps.R;
import co.edu.javeriana.tesispruebagps.account.AccountController;
import co.edu.javeriana.tesispruebagps.universal.UniversalTestValues;

public class SharingQRActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sharing_qr);

        Bundle extras = getIntent().getExtras();
        AccountController accountController = (AccountController) extras.getSerializable("account");
        String userID = accountController.getUserID();
        ImageView qrCode = (ImageView) findViewById(R.id.sharingQRQR);
        QRCodeWriter writer = new QRCodeWriter();
        try {
            BitMatrix bitMatrix = writer.encode(userID, BarcodeFormat.QR_CODE, 512, 512);
            int width = bitMatrix.getWidth();
            int height = bitMatrix.getHeight();
            Bitmap bmp = Bitmap.createBitmap(width, height, Bitmap.Config.RGB_565);
            for(int x = 0; x < width; ++x) {
                for(int y = 0; y < height; ++y) {
                    bmp.setPixel(x, y, bitMatrix.get(x, y) ? Color.BLACK : Color.WHITE);
                }
            }
            qrCode.setImageBitmap(bmp);
        } catch (WriterException e) {
            UniversalTestValues.debug(getClass(), e.toString());
        }
    }
}
