package co.edu.javeriana.tesispruebagps.controllers;

import com.parse.ParseException;
import com.parse.ParseObject;
import com.parse.ParseQuery;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import co.edu.javeriana.tesispruebagps.dto.Reservation;

/**
 * Gestiona las acciones de MyReservationActivity.
 * Created by Santiago on 29/10/2015.
 */
public class MyReservationsController {
    /**
     * Reservas hechas por el usuario.
     */
    List<Reservation> myReservations;

    /**
     * Obtiene las reservas realizadas por el usuario.
     * @param userID ID del usuario.
     * @throws ParseException Lanza una excepción cuando ocurre un fallo al realizar una consulta a
     * Parse.
     */
    public MyReservationsController(String userID) throws ParseException {
        ParseQuery<ParseObject> myUserQuery;
        ParseObject myParseUser;
        ParseQuery<ParseObject> reservationQuery;
        List<ParseObject> myReservationsParseForm;

        ParseObject falseArriveBlock;
        ParseObject falseDepartureBlock;
        ParseQuery arraiveBlockQuery;
        ParseQuery deparuteBlockQuery;
        ParseObject arraiveBlock;
        ParseObject departureBlock;
        ParseObject falseParking;
        ParseQuery<ParseObject> parkingQuery;
        ParseObject parking;

        String parseID;
        int state;
        String parkingName;
        Date arriveTime;
        Date departureTime;
        Reservation reservation;

        // Obtiene el usuario
        myUserQuery = ParseQuery.getQuery("_User");
        myParseUser = myUserQuery.get(userID);

        // Obtiene las reservas
        reservationQuery = ParseQuery.getQuery("Reserva");
        reservationQuery.whereEqualTo("cliente", myParseUser);
        myReservationsParseForm = reservationQuery.find();
        myReservations = new ArrayList<>();

        // Consulta la información de las reservas
        for( ParseObject myReservation : myReservationsParseForm ) {
            /*
            Por problemas en el uso de consultados a Parse, de cada reserva consulta el horario
            relacionado y de este el parqueadero relacionado.
             */
            falseArriveBlock = myReservation.getParseObject("bloqueInicio");
            falseDepartureBlock = myReservation.getParseObject("bloqueFin");
            arraiveBlockQuery = ParseQuery.getQuery("HorarioReserva");
            deparuteBlockQuery = ParseQuery.getQuery("HorarioReserva");
            arraiveBlock = arraiveBlockQuery.get(falseArriveBlock.getObjectId());
            departureBlock = deparuteBlockQuery.get(falseDepartureBlock.getObjectId());
            falseParking = arraiveBlock.getParseObject("parqueadero");
            parkingQuery = ParseQuery.getQuery("Parqueadero");
            parking = parkingQuery.get(falseParking.getObjectId());

            parseID = myReservation.getObjectId();
            state = myReservation.getInt("estado");
            parkingName = parking.getString("nombre");
            arriveTime = arraiveBlock.getDate("horaInicio");
            departureTime = departureBlock.getDate("horaFin");

            reservation = new Reservation(parseID, state, parkingName, arriveTime, departureTime);
            myReservations.add(reservation);
        }
    }

    /**
     *
     * @return Las reservas realizadas por el usuario.
     */
    public List<Reservation> getMyReservations() {
        return myReservations;
    }
}
