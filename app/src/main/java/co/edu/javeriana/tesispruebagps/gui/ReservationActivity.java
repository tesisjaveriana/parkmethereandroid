package co.edu.javeriana.tesispruebagps.gui;

import android.annotation.TargetApi;
import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Pair;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.parse.ParseException;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;
import java.util.Locale;

import co.edu.javeriana.tesispruebagps.R;
import co.edu.javeriana.tesispruebagps.account.AccountController;
import co.edu.javeriana.tesispruebagps.controllers.ReservationController;
import co.edu.javeriana.tesispruebagps.dto.ParkingInfo;
import co.edu.javeriana.tesispruebagps.notification.TimerNotification;
import co.edu.javeriana.tesispruebagps.universal.UniversalTestValues;

public class ReservationActivity extends AppCompatActivity {
    private ReservationController reservationController;
    private AccountController accountController;
    private Spinner arriveSpinner;
    private Spinner exitSpinner;
    private TextView balanceText;

    @TargetApi(Build.VERSION_CODES.JELLY_BEAN_MR1)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_reservation);

        final Bundle parameters = getIntent().getExtras();
        ParkingInfo selectedParking = (ParkingInfo) parameters.getSerializable("info");
        accountController = (AccountController) parameters.getSerializable("account");
        try {
            reservationController = new ReservationController(selectedParking);

            TextView parkingNameText = (TextView) findViewById(R.id.reservationParkingNameText);
            TextView priceText = (TextView) findViewById(R.id.reservationPriceText);
            TextView avaliableSlotsText = (TextView) findViewById(R.id.reservationAvaliableSlotsText);
            arriveSpinner = (Spinner) findViewById(R.id.reservationAvaliableScheduleSpinner);
            exitSpinner = (Spinner) findViewById(R.id.reservationExitTimeSpinner);
            balanceText = (TextView) findViewById(R.id.reservationEstimatedBalanceText);
            ListView departureListView = (ListView) findViewById(R.id.reservationDepartureList);
            Button reservationButton = (Button) findViewById(R.id.reservationReservationButton);

            List<Date> departureList = reservationController.getDepartureList();
            List<Pair<String,String>> avaliabeSchedule = reservationController.getAvalibleSchedule();
            List<CharSequence> startHours = new ArrayList<>();
            List<CharSequence> finishHours = new ArrayList<>();
            List<CharSequence> departureData = new ArrayList<>();
            ArrayAdapter<CharSequence> startHoursAdapter;
            ArrayAdapter<CharSequence> finishHoursAdapter;
            ArrayAdapter<CharSequence> departureAdapter;

            if(departureList.isEmpty()) {
                departureData.add("No hay salidas compartidas.");
            } else {
                for (Date departureHour : departureList) {
                    String departureShow = new SimpleDateFormat("HH:mm", Locale.getDefault()).format(departureHour);
                    departureData.add(departureShow);
                }
            }
            departureAdapter = new ArrayAdapter<>(this, android.R.layout.simple_list_item_1, departureData);
            departureListView.setAdapter(departureAdapter);

            parkingNameText.setText(selectedParking.getNombre());
            priceText.setText(Double.toString(selectedParking.getPrice()));
            avaliableSlotsText.setText(Integer.toString(selectedParking.getAvaliableSlots()));

            for (Pair<String, String> blockSchedule : avaliabeSchedule) {
                startHours.add(blockSchedule.first);
                finishHours.add(blockSchedule.second);
            }
            startHoursAdapter = new ArrayAdapter<>(this, android.R.layout.simple_spinner_dropdown_item, startHours);
            finishHoursAdapter = new ArrayAdapter<>(this, android.R.layout.simple_spinner_dropdown_item, finishHours);
            arriveSpinner.setAdapter(startHoursAdapter);
            exitSpinner.setAdapter(finishHoursAdapter);
            arriveSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {

                @Override
                public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                    String arriveTime = (String) ((TextView) view).getText();
                    String departureTime = exitSpinner.getSelectedItem().toString();
                    double price = reservationController.simulatePrice(arriveTime, departureTime);
                    balanceText.setText("$ " + Double.toString(price));
                }

                @Override
                public void onNothingSelected(AdapterView<?> parent) {

                }
            });
            exitSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {

                @Override
                public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                    String arriveTime = arriveSpinner.getSelectedItem().toString();
                    String departureTime = (String) ((TextView) view).getText();
                    double price = reservationController.simulatePrice(arriveTime, departureTime);
                    balanceText.setText("$ " + Double.toString(price));
                }

                @Override
                public void onNothingSelected(AdapterView<?> parent) {

                }
            });

            reservationButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    String arriveString = arriveSpinner.getSelectedItem().toString();
                    String exitString = exitSpinner.getSelectedItem().toString();
                    try {
                        if (reservationController.makeReservation(arriveString, exitString, accountController.getUserID())) {
                            try {
                                SimpleDateFormat df = new SimpleDateFormat("HH:mm", Locale.getDefault());
                                Date arraiveDate = df.parse(arriveSpinner.getSelectedItem().toString());
                                Date exitDate = df.parse(exitSpinner.getSelectedItem().toString());
                                Calendar arriveCalendar = new GregorianCalendar();
                                Calendar exitCalendar = new GregorianCalendar();
                                AlarmManager alarmManager = (AlarmManager) getSystemService(ALARM_SERVICE);
                                Calendar whenCalendar;
                                long when;
                                Intent arriveIntent;
                                Intent exitIntent;
                                Bundle arriveExtras;
                                Bundle exitExtras;
                                PendingIntent arrivePending;
                                PendingIntent exitPending;

                                arriveCalendar.setTime(arraiveDate);
                                exitCalendar.setTime(exitDate);

                                whenCalendar = Calendar.getInstance();
                                whenCalendar.set(Calendar.HOUR_OF_DAY, arriveCalendar.get(Calendar.HOUR_OF_DAY));
                                whenCalendar.set(Calendar.MINUTE, arriveCalendar.get(Calendar.MINUTE));
                                whenCalendar.set(Calendar.SECOND, 0);
                                when = whenCalendar.getTimeInMillis() - (10 * 60 * 1000);
                                arriveIntent = new Intent(ReservationActivity.this, TimerNotification.class);
                                arriveExtras = new Bundle();
                                arriveExtras.putString("notificationMessage", "Recuerde que a las " + df.format(arraiveDate) + " tiene una reserva.");
                                arriveIntent.putExtras(arriveExtras);
                                arrivePending = PendingIntent.getService(getApplicationContext(), 0, arriveIntent, 0);
                                alarmManager.set(AlarmManager.RTC, when, arrivePending);

                                whenCalendar = Calendar.getInstance();
                                whenCalendar.set(Calendar.HOUR_OF_DAY, exitCalendar.get(Calendar.HOUR_OF_DAY));
                                whenCalendar.set(Calendar.MINUTE, exitCalendar.get(Calendar.MINUTE));
                                whenCalendar.set(Calendar.SECOND, 0);
                                when = whenCalendar.getTimeInMillis() - (10 * 60 * 1000);
                                exitIntent = new Intent(ReservationActivity.this, TimerNotification.class);
                                exitExtras = new Bundle();
                                exitExtras.putString("notificationMessage", "Recuerde que a las " + df.format(exitDate) + " termina su reserva.");
                                exitIntent.putExtras(exitExtras);
                                exitPending = PendingIntent.getService(getApplicationContext(), 1, exitIntent, 0);
                                alarmManager.set(AlarmManager.RTC, when, exitPending);

                                Bundle extras = new Bundle();
                                extras.putSerializable("account", accountController);
                                Intent inMyReservations = new Intent(ReservationActivity.this, MyReservationsActivity.class);
                                inMyReservations.putExtras(extras);
                                startActivity(inMyReservations);
                                finish();
                            } catch (java.text.ParseException e1) {
                                Toast.makeText(ReservationActivity.this, "No se pudo hacer la reserva, intentelo nuevamente", Toast.LENGTH_SHORT).show();
                                UniversalTestValues.debug(this.getClass(), e1.toString());
                            }
                        } else {
                            UniversalTestValues.debug(this.getClass(), "Reserva inválida");
                        }
                    } catch (ParseException e) {
                        Toast.makeText(ReservationActivity.this, "No se pudo hacer la reserva, intentelo nuevamente", Toast.LENGTH_SHORT).show();
                        UniversalTestValues.debug(getClass(), e.toString());
                    }
                }
            });
        } catch (ParseException e) {
            Toast.makeText(ReservationActivity.this, "No se pudo iniciar la estadia, intentelo nuevamente", Toast.LENGTH_SHORT).show();
            UniversalTestValues.debug(this.getClass().getName(),e.toString());
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_reservation, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
