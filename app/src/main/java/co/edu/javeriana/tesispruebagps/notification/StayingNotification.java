package co.edu.javeriana.tesispruebagps.notification;

import android.annotation.TargetApi;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.app.TaskStackBuilder;
import android.content.Context;
import android.content.Intent;
import android.location.LocationManager;
import android.os.Build;
import android.os.Bundle;
import android.os.IBinder;
import android.support.annotation.Nullable;

import com.parse.FunctionCallback;
import com.parse.ParseCloud;
import com.parse.ParseException;
import com.parse.ParseObject;
import com.parse.ParseQuery;

import java.util.Calendar;
import java.util.HashMap;
import java.util.Map;

import co.edu.javeriana.tesispruebagps.account.AccountController;
import co.edu.javeriana.tesispruebagps.dto.ParkingInfo;
import co.edu.javeriana.tesispruebagps.gps.GPSLocationListener;
import co.edu.javeriana.tesispruebagps.gps.GPSObserver;
import co.edu.javeriana.tesispruebagps.gui.StayingActivity;
import co.edu.javeriana.tesispruebagps.universal.UniversalPersistence;
import co.edu.javeriana.tesispruebagps.universal.UniversalTestValues;

/**
 * Verifica cuando el
 * Created by Santiago on 19/11/2015.
 */
public class StayingNotification extends Service implements GPSObserver {
    /**
     * LocationListener que indica cuando hay un cambio de posición.
     */
    private GPSLocationListener gpsLocationListener;
    /**
     * Gestor que provee servicios de localización.
     */
    private LocationManager locationManager;
    private ParkingInfo lastParking;
    private ParseObject stayingParseForm;
    private AccountController accountController;

    /**
     * Activa el servicio GPS.
     */
    private void activateGPSListener() {
        locationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
        gpsLocationListener = new GPSLocationListener();
        gpsLocationListener.addObserver(this);
        locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 5000, 10,
                gpsLocationListener);
    }

    @Override
    public void onCreate() {
        try {
            UniversalPersistence.setSharedPreferences(getApplicationContext());
            String lastParkingID = UniversalPersistence.getLastParking();
            if(lastParkingID != null) {
                lastParking = new ParkingInfo(lastParkingID);
            }
            String stayingID = UniversalPersistence.getStayID();
            if (stayingID != null) {
                ParseQuery stayingQuery = ParseQuery.getQuery("Estadia");
                stayingParseForm = stayingQuery.get(stayingID);
            }
            accountController = new AccountController();
            activateGPSListener();
        } catch (ParseException e) {
            UniversalTestValues.debug(getClass(), e.toString());
            stopSelf();
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        locationManager.removeUpdates(gpsLocationListener);
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        if(intent != null) {
            Bundle extras = intent.getExtras();
            String command = extras.getString("command");

            switch(command) {
                case "init":
                    accountController = (AccountController) extras.getSerializable("account");
                    break;
                case "staying":
                    if(lastParking != null) {
                        Calendar departureHour = (Calendar) extras.getSerializable("departureHour");
                        ParseQuery<ParseObject> userQuery = ParseQuery.getQuery("_User");
                        ParseObject user;

                        ParkingInfo stayingParking = lastParking;
                        try {
                            user = userQuery.get(accountController.getUserID());
                            stayingParseForm = new ParseObject("Estadia");
                            stayingParseForm.put("parqueadero", stayingParking.getParseForm());
                            stayingParseForm.put("horaSalida", departureHour.getTime());
                            stayingParseForm.put("usuario", user);
                            stayingParseForm.save();
                            UniversalPersistence.setStayID(stayingParseForm.getObjectId());
                        } catch (NullPointerException | ParseException e) {
                            UniversalTestValues.debug(this.getClass(), e.toString());
                        }
                    }
                    break;
                case "pay":
                    stayingParseForm.deleteInBackground();
                    UniversalPersistence.setStayID(null);
                    break;
            }
        }
        return START_STICKY;
    }

    @TargetApi(Build.VERSION_CODES.JELLY_BEAN)
    private void makeNotification() {
        Bundle extras = new Bundle();
        extras.putSerializable("account", accountController);
        Intent inContadorNotificacion = new Intent( StayingNotification.this, StayingActivity.class);
        inContadorNotificacion.putExtras(extras);
        TaskStackBuilder stackBuilder = TaskStackBuilder.create(getApplicationContext());
        stackBuilder.addNextIntent(inContadorNotificacion);
        PendingIntent resultPendingIntent = stackBuilder.getPendingIntent(0, PendingIntent.FLAG_UPDATE_CURRENT);
        String message = "Haz entrado a "
                + lastParking.getNombre()
                + " ¿Desea compartir una hora de salida?";
        Notification.Builder mBuilder = new Notification
                .Builder(getApplicationContext())
                .setSmallIcon(android.R.drawable.ic_dialog_map)
                .setContentTitle("ParkMeThere!")
                .setContentText(message)
                .setContentIntent(resultPendingIntent)
                .setAutoCancel(true);

        NotificationManager mNotificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);

        Notification theNotification = mBuilder.build();
        theNotification.defaults |= Notification.DEFAULT_SOUND;
        theNotification.defaults |= Notification.DEFAULT_VIBRATE;

        mNotificationManager.notify(1, theNotification);
    }

    @Override
    public void notifyNewPosition(float lat, float lng) {
        Map<String, Float> params = new HashMap<>();

        params.put("latitude", lat);
        params.put("longitude", lng);
        ParseCloud.callFunctionInBackground("isInsideParking", params, new FunctionCallback<String>() {
            @TargetApi(Build.VERSION_CODES.JELLY_BEAN)
            @Override
            public void done(String parkingID, ParseException e) {
                if(e == null) {
                    try {
                        boolean firstEntrance = lastParking == null && parkingID != null;
                        boolean newEntrance = parkingID != null && lastParking != null && !parkingID.equals(lastParking.getObjectID());
                        if (firstEntrance || newEntrance) {
                            lastParking = new ParkingInfo(parkingID);
                            UniversalPersistence.setLastParking(parkingID);
                            makeNotification();
                        }
                    } catch (ParseException e1) {
                        UniversalTestValues.debug(getClass(), e1.toString());
                    }
                } else {
                    UniversalTestValues.debug(getClass(), e.toString());
                }
            }
        });
    }

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }
}
