package co.edu.javeriana.tesispruebagps.parkingsearcher;

import com.google.android.gms.maps.model.LatLng;

import com.parse.ParseException;
import java.util.List;

import co.edu.javeriana.tesispruebagps.dto.ParkingInfo;

/**
 * Interfaz que deben implementar los observadores a ParkinkSearcherController.
 * Created by Santiago on 31/10/2015.
 */
public interface ParkingSearcherObserver {
    /**
     * Notifica los límites y ruta a un parqueadero seleccionado.
     * @param limits Límites del parqueadero.
     * @param route Ruta al parqueadero.
     */
    void notifyParkingLimitsAndRoute(List<LatLng> limits, List<LatLng> route);

    /**
     * Notifica los parqueaderos cercanos de acuerdo a una posición y radio dadas.
     * @param nearParking Parqueaderos cercanos.
     */
    void notifyNearParkingTolerance(List<ParkingInfo> nearParking);

    void notifyNearParking(List<ParkingInfo> nearParking);

    /**
     * Notifica los parqueaderos visibles de acuerdo auna región dada.
     * @param visibleParking Parqueaderos visibles.
     */
    void notifyVisibleParking(List<ParkingInfo> visibleParking);

    /**
     * Notifica a los observadores de un error en la consulta realizada.
     * @param e
     */
    void notifyException(Exception e);
}
