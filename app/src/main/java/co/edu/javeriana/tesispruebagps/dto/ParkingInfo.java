package co.edu.javeriana.tesispruebagps.dto;

import com.google.android.gms.maps.model.LatLng;
import com.parse.ParseException;
import com.parse.ParseGeoPoint;
import com.parse.ParseObject;
import com.parse.ParseQuery;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/*
 * Created by Santiago on 23/10/2015.
 */
public class ParkingInfo implements Serializable {
    private String objectID;
    private String nombre;
    private List<Double> centroid;
    private double price;
    private int avaliableSlots;

    public ParkingInfo(String objectID) throws ParseException {
        this.objectID = objectID;
        ParseObject parseParking = this.getParseForm();
        this.nombre = parseParking.getString("nombre");
        ParseGeoPoint parseCentroid = parseParking.getParseGeoPoint("centroide");
        centroid = new ArrayList<>();
        centroid.add(parseCentroid.getLatitude());
        centroid.add(parseCentroid.getLongitude());
        this.price = parseParking.getDouble("tarifa");
        this.avaliableSlots = parseParking.getInt("cuposDisponibles");
    }

    public ParkingInfo(ParseObject parseForm) {
        this.objectID = parseForm.getObjectId();
        this.nombre = parseForm.getString("nombre");
        ParseGeoPoint parseCentroid = parseForm.getParseGeoPoint("centroide");
        centroid = new ArrayList<>();
        centroid.add(parseCentroid.getLatitude());
        centroid.add(parseCentroid.getLongitude());
        this.price = parseForm.getDouble("tarifa");
        this.avaliableSlots = parseForm.getInt("cuposDisponibles");
    }

    public ParseObject getParseForm() throws ParseException {
        ParseQuery<ParseObject> parkingQuery = ParseQuery.getQuery("Parqueadero");
        return parkingQuery.get(objectID);
    }

    public String getObjectID() {
        return objectID;
    }

    public String getNombre() {
        return nombre;
    }

    public LatLng getCentroid() {
        return new LatLng(centroid.get(0), centroid.get(1));
    }

    public double getPrice() {
        return price;
    }

    public int getAvaliableSlots() {
        return avaliableSlots;
    }

    public boolean isReservationAvaliable() throws ParseException {
        ParseQuery<ParseObject> scheduleQuery = ParseQuery.getQuery("HorarioReserva");
        scheduleQuery.whereEqualTo("parqueadero", getParseForm());
        scheduleQuery.whereGreaterThan("cuposDisponibles", 0);
        return scheduleQuery.count() != 0;
    }

    public boolean isDepartureInfo() throws ParseException {
        ParseQuery<ParseObject> nextDepartueQuery;

        nextDepartueQuery = ParseQuery.getQuery("Estadia");
        nextDepartueQuery.whereEqualTo("parqueadero", getParseForm());
        nextDepartueQuery.whereNotEqualTo("horaSalida", null);
        return nextDepartueQuery.count() != 0;
    }

    public List<Date> getDepartureList() throws ParseException {
        ParseQuery<ParseObject> departueQuery;
        List<ParseObject> resultsParse;
        List<Date> results;
        Date newDeparture;

        departueQuery = ParseQuery.getQuery("Estadia");
        departueQuery.whereEqualTo("parqueadero", getParseForm());
        departueQuery.whereNotEqualTo("horaSalida", null);
        resultsParse = departueQuery.find();

        results = new ArrayList<>();
        for(ParseObject deparute : resultsParse) {
            newDeparture = deparute.getDate("horaSalida");
            results.add(newDeparture);
        }

        return results;
    }
}
