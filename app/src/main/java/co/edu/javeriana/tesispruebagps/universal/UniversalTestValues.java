package co.edu.javeriana.tesispruebagps.universal;

import android.util.Log;

/*
 * Created by Santiago Medina on 27/08/2015.
 */
public class UniversalTestValues
{
    public static final float zoomLevel = 15.0f;

    public static void debug( String tagName, String value ) {
        Log.d( tagName, value );
    }
    public static void debug( Class originClass, String value ) {
        Log.d(originClass.getName(), value );
    }
}
