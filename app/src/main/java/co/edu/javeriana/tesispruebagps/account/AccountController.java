package co.edu.javeriana.tesispruebagps.account;

import com.parse.ParseException;
import com.parse.ParseObject;
import com.parse.ParseUser;

import java.io.Serializable;

import co.edu.javeriana.tesispruebagps.universal.UniversalPersistence;

/**
 * Gestiona la cuenta del usuario de la aplicación.
 * Created by Santiago on 13/11/2015.
 */
public class AccountController implements Serializable {
    private static final String firstNameAttribute = "firstName";
    private static final String lastNameAttribute = "lastName";

    /**
     * ID del usuario.
     */
    private String userID;

    public AccountController() {
        userID = UniversalPersistence.getUserID();
    }

    public String getUserID() {
        return userID;
    }

    /**
     * Registra un usuario nuevo.
     * @param username Nombre de usuario.
     * @param password Contraseña de la cuenta.
     * @param confirmPassword Rectificación de la contraseña.
     * @param firstName Nombre del usuario.
     * @param lastName Apellido del usuario.
     * @param email Correo electrónico del usuario.
     * @return Retorna true si las contraseñas coinciden y false en caso contrario.
     * @throws ParseException Lanza una excepción cuando hay una falla en la consulta a parse.
     */
    public boolean signup(String username, String password, String confirmPassword,
                          String firstName, String lastName, String email) throws ParseException {
        if(!password.equals(confirmPassword)) {
            return false;
        }
        ParseUser user = new ParseUser();
        user.setUsername(username);
        user.setPassword(password);
        user.setEmail(email);
        user.put(firstNameAttribute, firstName);
        user.put(lastNameAttribute, lastName);
        user.signUp();
        userID = user.getObjectId();
        UniversalPersistence.setUserID(userID);

        ParseObject userRating = new ParseObject("Conductor");
        userRating.put("usuario", user);
        userRating.put("rating", 5.0);
        userRating.save();

        return true;
    }

    /**
     * Autentica el usuario al sistema.
     * @param username Nombre de usuario
     * @param password Contraseña del usuario.
     * @throws ParseException Lanza una excepción cuando hay un fallo en la consulta a Parse.
     */
    public void login(String username, String password) throws ParseException {
        ParseUser user = ParseUser.logIn(username, password);
        userID = user.getObjectId();
        UniversalPersistence.setUserID(userID);
    }

    /**
     * Desconecta al usuario del sistema.
     */
    public void logout() {
        userID = null;
        UniversalPersistence.setUserID(null);
        ParseUser.logOutInBackground();
    }
}
