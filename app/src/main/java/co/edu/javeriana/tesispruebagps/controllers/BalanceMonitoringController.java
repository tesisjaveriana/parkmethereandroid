package co.edu.javeriana.tesispruebagps.controllers;

import java.util.Calendar;

import co.edu.javeriana.tesispruebagps.universal.UniversalPersistence;

/**
 * Created by Santiago on 11/01/2016.
 * Monitorea el saldo actual de una estadia
 */
public class BalanceMonitoringController {
    private boolean monitoringState;
    private Calendar arraiveTime;
    private float price;

    public BalanceMonitoringController() {
        monitoringState = UniversalPersistence.isBalanceMonitoring();
        arraiveTime = UniversalPersistence.getMonitoringArraive();
        price = UniversalPersistence.getMonitoringPrice();
    }

    public boolean isMonitoring() {
        return monitoringState;
    }

    public Calendar getArraiveTime() {
        return arraiveTime;
    }

    public float getPrice() {
        return price;
    }

    public void startMonitoring(float price) {
        monitoringState = true;
        arraiveTime = Calendar.getInstance();
        this.price = price;
        updatePersistance();
    }

    public double getBalance() {
        long arraiveSeconds = arraiveTime.getTimeInMillis();
        long currentSeconds = Calendar.getInstance().getTimeInMillis();
        long diffMinutes = (currentSeconds - arraiveSeconds) / (1000 * 60);
        return diffMinutes * (double)price;
    }

    public void stopMonitoring() {
        monitoringState = false;
        arraiveTime = null;
        price = 0;
        updatePersistance();
    }

    private void updatePersistance() {
        UniversalPersistence.setBalanceMonitoring(monitoringState);
        UniversalPersistence.setMonitoringArraive(arraiveTime);
        UniversalPersistence.setMonitoringPrice(price);
    }
}
