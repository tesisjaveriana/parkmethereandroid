package co.edu.javeriana.tesispruebagps.controllers;

import com.google.android.gms.maps.model.LatLng;

import co.edu.javeriana.tesispruebagps.gps.GPSLocationListener;
import co.edu.javeriana.tesispruebagps.gps.GPSObserver;
import co.edu.javeriana.tesispruebagps.parkingsearcher.ParkingSearcherController;
import co.edu.javeriana.tesispruebagps.parkingsearcher.ParkingSearcherObserver;
import co.edu.javeriana.tesispruebagps.universal.UniversalPersistence;

/**
 * Gestiona la lógica de MapsActivity
 * Created by Santiago on 31/10/2015.
 */
public class MapsController {
    /**
     * Listener al GPS del celular.
     */
    private GPSLocationListener gpsLocationListener;
    /**
     * Gestiona la lógica para buscar parqueaderos.
     */
    private ParkingSearcherController parkingSearcherController;

    public MapsController() {
        this.gpsLocationListener = new GPSLocationListener();
        this.parkingSearcherController = new ParkingSearcherController();
    }

    /**
     *
     * @return Objeto que implementa LocationListener
     */
    public GPSLocationListener getGpsLocationListener() {
        return gpsLocationListener;
    }

    /**
     *
     * @return La última posición guardada por MapsActivity.
     */
    public LatLng getLastCoordinate() {
        return new LatLng(UniversalPersistence.getLastLatitude(),
                UniversalPersistence.getLastLongitude());
    }

    /**
     * Busca los límites de un parqueadero y traza la ruta hasta este. Luego notifica el resultado
     * a los observadores suscritos.
     * @param parkingName Nombre del parqueadero a buscar.
     * @param currentPosition Posición actual
     */
    public void lookUpParkingLimitsAndRoute(String parkingName, LatLng currentPosition) {
        parkingSearcherController.lookUpParkingLimitsAndRoute(parkingName, currentPosition);
    }

    /**
     * Busca los parqueaderos cercanos de acuerdo a una posición y un radio. Luego notifica los
     * resultados a los observados suscritos.
     * @param currentPosition Posición desde cual se busca los parqueaderos.
     * @param tolerance Radio al que se quiere buscar.
     */
    public void lookUpNearParkingTolerance(LatLng currentPosition, double tolerance) {
        parkingSearcherController.lookUpNearParkingTolerance(currentPosition, tolerance);
    }

    public void lookUpNearParking(LatLng currentPosition) {
        parkingSearcherController.lookUpNearParking(currentPosition);
    }

    /**
     * Busca los parqueaderos de acuerdo a una región visible. Luego notifica los resultados a los
     * observadores suscritos.
     * @param NE Punto superior derecho de la región.
     * @param SW Punto inferior izquierdo de la región.
     */
    public void lookUpVisibleParking(LatLng NE, LatLng SW) {
        parkingSearcherController.lookUpVisibleParking(NE, SW);
    }

    /**
     * Agrega un nuevo observador al cual notificar el resultado del cambio de posición.
     * @param newObserver Un nuevo observador.
     */
    public void addGPSObserver(GPSObserver newObserver) {
        gpsLocationListener.addObserver(newObserver);
    }

    /**
     * Agrega un nuevo observado al cual notificar los resultados relacionadas con búsquedas de
     * parqueaderos.
     * @param newObserver Un nuevo observador.
     */
    public void addParkingSearcherObserver(ParkingSearcherObserver newObserver) {
        parkingSearcherController.addObserver(newObserver);
    }
}
