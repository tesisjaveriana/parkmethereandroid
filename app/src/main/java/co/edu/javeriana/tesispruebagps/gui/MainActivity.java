package co.edu.javeriana.tesispruebagps.gui;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;

import co.edu.javeriana.tesispruebagps.R;
import co.edu.javeriana.tesispruebagps.account.AccountController;
import co.edu.javeriana.tesispruebagps.notification.StayingNotification;
import co.edu.javeriana.tesispruebagps.universal.UniversalPersistence;

public class MainActivity extends AppCompatActivity {
    private AccountController accountController;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Bundle extras = getIntent().getExtras();
        accountController = (AccountController) extras.getSerializable("account");

        Button returnButton = (Button) findViewById(R.id.mainReturnButton);
        Button myReservationsButton = (Button) findViewById(R.id.mainMyReservationsButton);
        Button chronometerButton = (Button) findViewById(R.id.mainChronometerButton);
        Button historyButton = (Button) findViewById(R.id.mainHistoryButton);
        Button optionsButton = (Button) findViewById(R.id.mainOptionsButton);
        Button helpButton = (Button) findViewById(R.id.mainHelpButton);
        Button logoutButton = (Button) findViewById(R.id.mainLogoutButton);
        Button aboutButton = (Button) findViewById(R.id.mainAboutButton);

        returnButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        myReservationsButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Bundle extras = new Bundle();
                extras.putSerializable("account", accountController);
                Intent intMyReservations = new Intent(MainActivity.this, MyReservationsActivity.class);
                intMyReservations.putExtras(extras);
                startActivity(intMyReservations);
            }
        } );

        chronometerButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent chronometerIntent = new Intent(MainActivity.this, ChronometerActivity.class);
                startActivity(chronometerIntent);
            }
        });

        historyButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Bundle extras = new Bundle();
                extras.putSerializable("account", accountController);
                Intent historyIntent = new Intent(MainActivity.this, HistoryActivity.class);
                historyIntent.putExtras(extras);
                startActivity(historyIntent);
            }
        });

        optionsButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Bundle extras = new Bundle();
                extras.putSerializable("account", accountController);
                Intent optionsIntent = new Intent(MainActivity.this, OptionsActivity.class);
                optionsIntent.putExtras(extras);
                startActivity(optionsIntent);
            }
        });

        helpButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent helpIntent = new Intent(MainActivity.this, HelpActivity.class);
                startActivity(helpIntent);
            }
        });

        logoutButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                accountController.logout();
                Intent responseResult = new Intent();
                responseResult.putExtra("EXIT", true);
                setResult(RESULT_OK, responseResult);
                finish();
                Intent stayingNotificationIntent = new Intent(MainActivity.this, StayingNotification.class);
                stopService(stayingNotificationIntent);
            }
        });

        aboutButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent aboutInet = new Intent(MainActivity.this, AboutActivity.class);
                startActivity(aboutInet);
            }
        });
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
