package co.edu.javeriana.tesispruebagps.gui;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TabHost;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.Polygon;
import com.google.android.gms.maps.model.PolygonOptions;
import com.google.android.gms.maps.model.Polyline;
import com.google.android.gms.maps.model.PolylineOptions;
import com.google.android.gms.maps.model.VisibleRegion;
import com.parse.GetCallback;
import com.parse.ParseException;
import com.parse.ParseObject;
import com.parse.ParseQuery;
import com.parse.ParseUser;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;
import java.util.TreeSet;

import co.edu.javeriana.tesispruebagps.R;
import co.edu.javeriana.tesispruebagps.account.AccountController;
import co.edu.javeriana.tesispruebagps.controllers.MapsController;
import co.edu.javeriana.tesispruebagps.customviews.HelpListAdapter;
import co.edu.javeriana.tesispruebagps.dto.ParkingInfo;
import co.edu.javeriana.tesispruebagps.gps.GPSObserver;
import co.edu.javeriana.tesispruebagps.parkingsearcher.ParkingSearcherObserver;
import co.edu.javeriana.tesispruebagps.universal.UniversalPersistence;
import co.edu.javeriana.tesispruebagps.universal.UniversalTestValues;

public class MapsActivity extends FragmentActivity implements GPSObserver, ParkingSearcherObserver {
    private static int[] images = {
            R.drawable.current_position,
            R.drawable.parking_icon,
            R.drawable.information_button,
            R.drawable.search_button
    };
    private static String[] tags = {
            "Posición actual.",
            "Parqueadero.",
            "Despliega la información del parqueadero. Además permite compartir una estadía y " +
                    "hacer una reserva en el parqueadero seleccionado si ofrece el servicio",
            "Busca los parqueaderos a la redonda de acuerdo al parámetro dado"
    };
    private static final int EXIT = 0;

    private MapsController mapsController;
    private AccountController accountController;

    private GoogleMap mMap; // Might be null if Google Play services APK is not available.
    private LocationManager locManager;
    private LocationListener locationListener;
    private Marker currentPosition;
    private Marker lastParkingSelected;
    private Map<String,Marker> visibleParking;
    private List<LatLng> parkingCoordinates;
    private TabHost tabHost;

    private Polygon parkingDraw;
    private Polyline routeLine;
    private ListView parkingList;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_maps);

        UniversalPersistence.setSharedPreferences(getApplicationContext());

        Bundle extras = getIntent().getExtras();
        accountController = (AccountController) extras.getSerializable("account");

        mapsController = new MapsController();
        mapsController.addParkingSearcherObserver(this);

        parkingCoordinates = null;
        lastParkingSelected = null;
        visibleParking = new TreeMap<>();

        tabHost = (TabHost) findViewById(R.id.mapsTabHost);
        tabHost.setup();

        TabHost.TabSpec tabMapa = tabHost.newTabSpec("tabMapa");
        tabMapa.setIndicator("Mapa");
        tabMapa.setContent(R.id.tab1);
        tabHost.addTab(tabMapa);

        TabHost.TabSpec tabBusqueda = tabHost.newTabSpec("tabBusqueda");
        tabBusqueda.setIndicator("Buscar");
        tabBusqueda.setContent(R.id.tab2);
        tabHost.addTab(tabBusqueda);

        TabHost.TabSpec tabHelp = tabHost.newTabSpec("tabHelp");
        tabHelp.setIndicator("Leyenda");
        tabHelp.setContent(R.id.tab3);
        tabHost.addTab(tabHelp);

        TabHost.TabSpec tabInfo = tabHost.newTabSpec("tabInfo");
        tabInfo.setIndicator("Mi cuenta");
        tabInfo.setContent(R.id.tab4);
        tabHost.addTab(tabInfo);

        Button searchButton = (Button) findViewById( R.id.mapsSearchButton );
        Button nearButton = (Button) findViewById(R.id.mapsNearButton);
        Button infoButton = (Button) findViewById( R.id.mapsInfoButton);
        Button menuButton = (Button) findViewById(R.id.mapsMenuButton);
        TextView usernameText = (TextView) findViewById(R.id.mapsUsernameText);
        TextView firstNameText = (TextView) findViewById(R.id.mapsFirstNameText);
        TextView lastNameText = (TextView) findViewById(R.id.mapsLastNameText);
        TextView emailText = (TextView) findViewById(R.id.mapsEmailText);
        TextView signupDateText = (TextView) findViewById(R.id.mapsSignUpDateText);

        ParseUser user = ParseUser.getCurrentUser();
        String username = user.getUsername();
        String firstName = user.getString("firstName");
        String lastName = user.getString("lastName");
        String email = user.getEmail();
        Date signupDate = user.getCreatedAt();
        String signUpDateString = new SimpleDateFormat("yyyy/MM/dd", Locale.getDefault()).format(signupDate);

        usernameText.setText(username);
        firstNameText.setText(firstName);
        lastNameText.setText(lastName);
        emailText.setText(email);
        signupDateText.setText(signUpDateString);
        ParseQuery<ParseObject> ratingQuery = ParseQuery.getQuery("Conductor");
        ratingQuery.whereEqualTo("usuario", user);
        ratingQuery.getFirstInBackground(new GetCallback<ParseObject>() {
            @Override
            public void done(ParseObject parseRating, ParseException e) {
                if (e == null) {
                    TextView ratingText = (TextView) findViewById(R.id.mapsRating);
                    double rating = parseRating.getDouble("rating");
                    String ratingString = Double.toString(rating) + "/10";
                    ratingText.setText(ratingString);
                } else {
                    Toast.makeText(MapsActivity.this, e.getMessage(), Toast.LENGTH_SHORT).show();
                }
            }
        });

        ListView helpList = (ListView) findViewById(R.id.mapsHelpList);
        helpList.setAdapter(new HelpListAdapter(getApplicationContext(), tags, images));

        parkingList = (ListView) findViewById( R.id.mapsParkingList );
        // Inicialización del mapa
        setUpMapIfNeeded();

        // Listener que carga la ruta y los límites del parqueadero seleccionado.
        parkingList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                TextView selectedParkingText = (TextView) view;
                mapsController.lookUpParkingLimitsAndRoute((String) selectedParkingText.getText(), currentPosition.getPosition());
            }
        });

        // Listener que busca los parqueaderos cercanos
        searchButton.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                EditText toleranceEdit = (EditText) findViewById(R.id.mapsToleranceText);
                String toleranceString = toleranceEdit.getText().toString();
                //mapsController.lookUpNearParking(currentPosition.getPosition(), Double.valueOf(toleranceEdit.getText().toString()));
                if(toleranceString.isEmpty()) {
                    Toast.makeText(getApplicationContext(), "Ingrese una radio para la búsqueda", Toast.LENGTH_SHORT).show();
                } else {
                    mapsController.lookUpNearParkingTolerance(currentPosition.getPosition(), Double.valueOf(toleranceString));
                }
            }
        });

        nearButton.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                mapsController.lookUpNearParking(currentPosition.getPosition());
            }
        });

        // Listener que muestra los parqueaderos dinámicamente
        mMap.setOnCameraChangeListener(new GoogleMap.OnCameraChangeListener() {
            @Override
            public void onCameraChange(CameraPosition cameraPosition) {
                VisibleRegion screenRegion = mMap.getProjection().getVisibleRegion();
                mapsController.lookUpVisibleParking(screenRegion.farRight, screenRegion.nearLeft);
            }
        });

        infoButton.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                boolean isInfoShown = true;
                if (lastParkingSelected != null) {
                    if (lastParkingSelected.isInfoWindowShown()) {
                        try {
                            ParkingInfo infoOfSelected = null;
                            for (Map.Entry<String, Marker> entry : visibleParking.entrySet()) {
                                if (entry.getValue().equals(lastParkingSelected)) {
                                    infoOfSelected = new ParkingInfo(entry.getKey());
                                    break;
                                }
                            }
                            Intent reservationIntent = new Intent(MapsActivity.this, ReservationActivity.class);
                            Bundle parameters = new Bundle();

                            parameters.putSerializable("info", infoOfSelected);
                            parameters.putSerializable("account", accountController);
                            reservationIntent.putExtras(parameters);
                            startActivity(reservationIntent);
                        } catch (ParseException e) {
                            Toast.makeText(MapsActivity.this, "Ocurrio un error consultado la información del parqueadero, intentelo nuevamente", Toast.LENGTH_SHORT).show();
                            UniversalTestValues.debug(this.getClass(), e.toString());
                        }
                    } else {
                        isInfoShown = false;
                    }
                } else {
                    isInfoShown = false;
                }
                if (!isInfoShown) {
                    Toast.makeText(MapsActivity.this, "No se ha seleccionado un parqueadero", Toast.LENGTH_SHORT).show();
                }
            }
        });

        menuButton.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                Bundle extras = new Bundle();
                extras.putSerializable("account", accountController);
                Intent mainIntent = new Intent(MapsActivity.this, MainActivity.class);
                mainIntent.putExtras(extras);
                startActivityForResult(mainIntent, EXIT);
            }
        });
    }

    @Override
    protected void onResume()
    {
        super.onResume();
        setUpMapIfNeeded();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        locManager.removeUpdates(locationListener);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == EXIT && resultCode == RESULT_OK && data != null
                && data.hasExtra("EXIT") && data.getBooleanExtra("EXIT", false)) {
            finish();
        }
    }

    /**
     * Sets up the map if it is possible to do so (i.e., the Google Play services APK is correctly
     * installed) and the map has not already been instantiated.. This will ensure that we only ever
     * call setUpMap() once when {@link #mMap} is not null.
     * <p/>
     * If it isn't installed {@link SupportMapFragment} (and
     * {@link com.google.android.gms.maps.MapView MapView}) will show a prompt for the user to
     * install/update the Google Play services APK on their device.
     * <p/>
     * A user can return to this FragmentActivity after following the prompt and correctly
     * installing/updating/enabling the Google Play services. Since the FragmentActivity may not
     * have been completely destroyed during this process (it is likely that it would only be
     * stopped or paused), {@link #onCreate(Bundle)} may not be called again so we should call this
     * method in {@link #onResume()} to guarantee that it will be called.
     */
    private void setUpMapIfNeeded()
    {
        // Do a null check to confirm that we have not already instantiated the map.
        if (mMap == null) {
            // Try to obtain the map from the SupportMapFragment.
            mMap = ((SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.map))
                    .getMap();
            // Check if we were successful in obtaining the map.
            if (mMap != null) {
                LatLng lastCoordinates;

                // Cargo mi última posición
                lastCoordinates = mapsController.getLastCoordinate();
                mMap.moveCamera(CameraUpdateFactory.newLatLng(lastCoordinates));
                mMap.moveCamera(CameraUpdateFactory.zoomTo(UniversalTestValues.zoomLevel));
                currentPosition = mMap.addMarker( (new MarkerOptions())
                        .position(lastCoordinates)
                        .icon(BitmapDescriptorFactory.fromResource(R.drawable.current_position)));

                // Inicializo el GPSListener
                locManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);

                mapsController.addGPSObserver(this);
                locationListener = mapsController.getGpsLocationListener();
                locManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 5000, 10,
                        locationListener);

                // Siempre se guarda el último marcador seleccionado.
                mMap.setOnMarkerClickListener( new GoogleMap.OnMarkerClickListener() {
                    @Override
                    public boolean onMarkerClick(Marker marker) {
                        lastParkingSelected = marker;
                        return false;
                    }
                } );
            }
        }
    }

    @Override
    public void notifyNewPosition(float lat, float lng) {
        LatLng currentPosition = new LatLng(lat, lng);
        mMap.moveCamera(CameraUpdateFactory.newLatLng(currentPosition));
        if(!this.currentPosition.getPosition().equals(currentPosition)) {
            this.currentPosition.remove();
            this.currentPosition = mMap.addMarker((new MarkerOptions())
                    .position(currentPosition)
                    .icon(BitmapDescriptorFactory.fromResource(R.drawable.current_position)));
        }
    }

    @Override
    public void notifyParkingLimitsAndRoute(List<LatLng> limits, List<LatLng> route) {
        PolygonOptions limitOptions = new PolygonOptions();
        PolylineOptions routeOptions = new PolylineOptions();

        if (parkingDraw != null) {
            parkingDraw.remove();
        }
        parkingCoordinates = limits;
        for (LatLng coordinate : parkingCoordinates) {
            limitOptions = limitOptions.add(coordinate);
        }
        parkingDraw = mMap.addPolygon(limitOptions.strokeColor(Color.RED).fillColor(Color.BLUE));

        if (routeLine != null) {
            routeLine.remove();
        }
        for(LatLng coordinate : route) {
            routeOptions.add(coordinate);
        }

        routeLine = mMap.addPolyline(routeOptions);
        tabHost.setCurrentTab(0);
    }

    @Override
    public void notifyNearParkingTolerance(List<ParkingInfo> nearParking) {
        List<String> displayData = new ArrayList<>();
        ArrayAdapter<String> parkingArray;
        int layoutRow = R.layout.parking_list_layout;

        if (nearParking.isEmpty()) {
            displayData.add("No hay parqueaderos cercanos");

        } else {
            for(ParkingInfo parkingInfo : nearParking) {
                displayData.add(parkingInfo.getNombre());
            }
        }
        parkingArray = new ArrayAdapter<>(getApplicationContext(), layoutRow, displayData);
        parkingList.setAdapter(parkingArray);
        parkingArray.notifyDataSetChanged();
    }

    @Override
    public void notifyNearParking(List<ParkingInfo> nearParking) {
        List<String> displayData = new ArrayList<>();
        ArrayAdapter<String> parkingArray;
        int layoutRow = R.layout.parking_list_layout;

        if (nearParking.isEmpty()) {
            displayData.add("No hay parqueaderos cercanos");

        } else {
            for(ParkingInfo parkingInfo : nearParking) {
                displayData.add(parkingInfo.getNombre());
            }
        }
        parkingArray = new ArrayAdapter<>(getApplicationContext(), layoutRow, displayData);
        parkingList.setAdapter(parkingArray);
        parkingArray.notifyDataSetChanged();
    }

    @Override
    public void notifyVisibleParking(List<ParkingInfo> results) {
        Set<String> parkingToRemove;
        String parkingID;
        double lat;
        double lng;
        LatLng parkingPosition;
        String name;
        int avaliableSlots;
        double price;
        String isDepartureString;
        String avaliableReservationString;
        String info;
        LatLng centroid;
        Marker aParking;

        parkingToRemove = new TreeSet<>(visibleParking.keySet());
        for( ParkingInfo parking : results ) {
            parkingID = parking.getObjectID();
            if( visibleParking.get(parkingID) != null ) {
                parkingToRemove.remove(parkingID);
            } else {
                isDepartureString = "";
                avaliableReservationString = "";
                centroid = parking.getCentroid();
                lat = centroid.latitude;
                lng = centroid.longitude;
                parkingPosition = new LatLng( lat, lng );
                name = parking.getNombre();
                avaliableSlots = parking.getAvaliableSlots();
                price = parking.getPrice();
                try {
                    if(parking.isReservationAvaliable()) {
                        avaliableReservationString = "RD";
                    }
                    if(parking.isDepartureInfo()) {
                        isDepartureString = "SD";
                    }
                } catch (ParseException e) {
                    UniversalTestValues.debug(this.getClass(), e.toString());
                }
                info = "Cupos:" + Integer.toString(avaliableSlots)
                        + " Tarifa:$" + Double.toString(price) + "/min"
                        + ' ' + isDepartureString
                        + ' ' + avaliableReservationString;
                aParking = mMap.addMarker(new MarkerOptions()
                        .position(parkingPosition)
                        .title(name)
                        .snippet(info)
                        .icon(BitmapDescriptorFactory.fromResource(R.drawable.parking_icon)));
                visibleParking.put(parkingID, aParking);
            }
        }
        for(String objectID : parkingToRemove) {
            visibleParking.get(objectID).remove();
            visibleParking.remove(objectID);
        }
    }

    @Override
    public void notifyException(Exception e) {
        Toast.makeText(MapsActivity.this, "Ocurrio un error, intentelo nuevamente", Toast.LENGTH_SHORT).show();
        UniversalTestValues.debug(this.getClass(), e.toString());
    }
}
