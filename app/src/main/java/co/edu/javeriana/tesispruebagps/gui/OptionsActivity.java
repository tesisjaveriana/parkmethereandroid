package co.edu.javeriana.tesispruebagps.gui;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.Switch;

import com.parse.ParseUser;

import co.edu.javeriana.tesispruebagps.R;
import co.edu.javeriana.tesispruebagps.account.AccountController;
import co.edu.javeriana.tesispruebagps.notification.StayingNotification;
import co.edu.javeriana.tesispruebagps.universal.UniversalPersistence;

public class OptionsActivity extends AppCompatActivity {
    private AccountController accountController;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_options);

        Bundle extras = getIntent().getExtras();
        accountController = (AccountController) extras.getSerializable("account");
        ParseUser user = ParseUser.getCurrentUser();

        Switch sharingSwitch = (Switch) findViewById(R.id.optionsSharingSwitch);
        Button sharingButton = (Button) findViewById(R.id.optionsSharingButton);

        sharingSwitch.setChecked(UniversalPersistence.getSharingState());
        sharingSwitch.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean state) {
                Bundle extras = new Bundle();
                extras.putString("command", "init");
                extras.putSerializable("account", accountController);
                Intent stayingNotificationIntent = new Intent(OptionsActivity.this, StayingNotification.class);
                stayingNotificationIntent.putExtras(extras);
                if (state) {
                    startService(stayingNotificationIntent);
                } else {
                    stopService(stayingNotificationIntent);
                }
                UniversalPersistence.setSharingState(state);
            }
        });

        sharingButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Bundle extras = new Bundle();
                extras.putSerializable("account", accountController);
                Intent sharingQRIntent = new Intent(OptionsActivity.this, SharingQRActivity.class);
                sharingQRIntent.putExtras(extras);
                startActivity(sharingQRIntent);
            }
        });
    }
}
