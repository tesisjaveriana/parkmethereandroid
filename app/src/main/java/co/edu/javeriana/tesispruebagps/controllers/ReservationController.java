package co.edu.javeriana.tesispruebagps.controllers;

import android.util.Pair;

import com.parse.ParseException;

import java.util.Date;
import java.util.List;

import co.edu.javeriana.tesispruebagps.dto.ParkingInfo;
import co.edu.javeriana.tesispruebagps.dto.ParkingSchedule;

/**
 * Gestiona la lógica de ReservationActivity.
 * Created by Santiago on 23/10/2015.
 */
public class ReservationController {
    /**
     * Horario de reservas del parqueadero.
     */
    private ParkingSchedule avalibleSchedule;

    /**
     * Consulta el horario de reservas del parqueadero.
     * @param selectedParking Parqueadero al cual se le va a realizar la consulta.
     * @throws ParseException Lanza una exception cuando hay un fallo en una consulta a Parse.
     */
    public ReservationController(ParkingInfo selectedParking) throws ParseException {
        this.avalibleSchedule = new ParkingSchedule(selectedParking);
    }

    /**
     *
     * @return El parqueadero al cual se le esta haciendo la consulta.
     */
    public ParkingInfo getParkingInfo() {
        return avalibleSchedule.getSelectedParking();
    }

    /**
     *
     * @return El horario de reservas del parqueadero.
     * @throws ParseException Lanza una excepción cuando hay un fallo en una consulta a Parse.
     */
    public List<Pair<String,String>> getAvalibleSchedule() throws ParseException {
        return avalibleSchedule.getAvaliableSchedule();
    }

    /**
     * Hace un recerva.
     * @param startTime Hora de inicio de la reserva.
     * @param finishTime Hora de finalización de la reserva.
     * @param userID ID dle usuario.
     * @return Retorna true cuando los parámetros ingresados son válido y hace una reserva y false
     * en el caso contrario.
     * @throws ParseException Lanza una excepción cuando hay un fallo en una consulta a Parse.
     */
    public boolean makeReservation(String startTime, String finishTime, String userID) throws ParseException {
        return avalibleSchedule.validateReservation(startTime, finishTime, userID);
    }

    /**
     * Genera un precio de acuerdo a una reserva propuesta.
     * @param startTime Hora de inicio de la reserva.
     * @param finishTime Hora de finalización de la reserva.
     * @return El precio generado.
     */
    public double simulatePrice(String startTime, String finishTime) {
        return avalibleSchedule.simulatePrice(startTime, finishTime);
    }

    public List<Date> getDepartureList() throws ParseException {
        return avalibleSchedule.getDepartureList();
    }
}
