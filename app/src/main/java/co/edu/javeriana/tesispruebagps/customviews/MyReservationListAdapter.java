package co.edu.javeriana.tesispruebagps.customviews;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import co.edu.javeriana.tesispruebagps.R;
import co.edu.javeriana.tesispruebagps.gui.QRActivity;

/**
 * Adaptador para mostrar reservas
 * Created by Santiago on 16/12/2015.
 */
public class MyReservationListAdapter extends BaseAdapter {
    private Context originActivity;
    private List<CharSequence> reservationsDetails;
    private List<String> reservationsIDS;
    private static LayoutInflater inflater = null;

    public MyReservationListAdapter(Context originActivity, List<CharSequence> reservationsDetails, List<String> reservationsIDS) {
        this.originActivity = originActivity;
        this.reservationsDetails = new ArrayList<>(reservationsDetails);
        this.reservationsIDS = new ArrayList<>(reservationsIDS);
        inflater = ( LayoutInflater )originActivity.getApplicationContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public int getCount() {
        return reservationsDetails.size();
    }

    @Override
    public Object getItem(int position) {
        return reservationsIDS.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        final int auxPosition = position;
        Holder holder = new Holder();
        View viewRow = inflater.inflate(R.layout.my_reservations_list_layout, null);

        holder.details = (TextView) viewRow.findViewById(R.id.myReservationsListText);
        holder.qrButton = (Button) viewRow.findViewById(R.id.myReservationsListButton);

        holder.details.setText(reservationsDetails.get(position));
        holder.details.setTextColor(originActivity.getResources().getColor(R.color.app_text_color));
        holder.qrButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Bundle extras = new Bundle();
                extras.putString("reservationID", reservationsIDS.get(auxPosition));
                Intent QRIntent = new Intent(originActivity, QRActivity.class);
                QRIntent.putExtras(extras);
                originActivity.startActivity(QRIntent);
            }
        });

        return viewRow;
    }

    public class Holder {
        TextView details;
        Button qrButton;
    }
}
