package co.edu.javeriana.tesispruebagps.dto;

import java.util.Date;

/**
 * Representa una reserva realizada por el usuario.
 * Created by Santiago on 29/10/2015.
 */
public class Reservation {
    /**
     * ID en Parse
     */
    private String parseID;
    /**
     * Estado de la reserva.
     */
    private int estado;
    /**
     * Nombre del parqueadero donde se realizó la reserva.
     */
    private String nombreParqueadero;
    /**
     * Hora de inicio de la reserva.
     */
    private Date horaLlegada;
    /**
     * Hora de finalización de la reserva.
     */
    private Date horaSalida;

    /**
     *
     * @param estado Estado de la reserva.
     * @param nombreParqueadero Nombre del parqueadero.
     * @param horaLlegada Hora de inicio de la reserva.
     * @param horaSalida Hora de finalización de la reserva.
     */
    public Reservation(String parseID, int estado, String nombreParqueadero, Date horaLlegada, Date horaSalida) {
        this.parseID = parseID;
        this.estado = estado;
        this.nombreParqueadero = nombreParqueadero;
        this.horaLlegada = horaLlegada;
        this.horaSalida = horaSalida;
    }

    /**
     *
     * @return ID en Parse.
     */
    public String getParseID() {
        return parseID;
    }

    /**
     *
     * @return El estado de la reserva.
     */
    public int getEstado() {
        return estado;
    }

    /**
     *
     * @return Nombre del parqueadero donde se hizo la reserva.
     */
    public String getNombreParqueadero() {
        return nombreParqueadero;
    }

    /**
     *
     * @return Hora de inicio de la reserva.
     */
    public Date getHoraLlegada() {
        return (Date) horaLlegada.clone();
    }

    /**
     *
     * @return Hora de finalización de la reserva.
     */
    public Date getHoraSalida() {
        return (Date) horaSalida.clone();
    }
}
