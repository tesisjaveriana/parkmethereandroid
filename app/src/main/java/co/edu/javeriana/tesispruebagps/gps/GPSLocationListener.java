package co.edu.javeriana.tesispruebagps.gps;

import android.location.Location;
import android.location.LocationListener;
import android.os.Bundle;

import java.util.ArrayList;
import java.util.List;

import co.edu.javeriana.tesispruebagps.universal.UniversalPersistence;

/**
 * Created by Santiago on 16/08/2015.
 *
 * Comunicación con servicio GPS.
 */
public class GPSLocationListener implements LocationListener
{
    /**
     * Observadores suscritos a quienes se les notifica un cambio de estado.
     */
    private List<GPSObserver> observers;

    public GPSLocationListener() {
        observers = new ArrayList<>();
    }

    /**
     * Notifica a los observadores suscritos de un nuevo cambio de posición.
     * @param lat Nueva latitud.
     * @param lng Nueva longitud.
     */
    private void notifyObservers(float lat, float lng) {
        for( GPSObserver observer : observers ) {
            observer.notifyNewPosition(lat, lng);
        }
    }

    /**
     * Agrega un nuevo observado
     * @param newObserver Un nuevo observador.
     */
    public void addObserver(GPSObserver newObserver) {
        observers.add(newObserver);
    }

    /**
     * Método que se llama cuando hay un nuevo cambio de posición.
     * @param location Nueva posición.
     */
    @Override
    public void onLocationChanged(Location location) {
        float lat = (float)location.getLatitude();
        float lng = (float)location.getLongitude();
        UniversalPersistence.setLastPosition(lat, lng);
        notifyObservers( lat, lng );
    }

    @Override
    public void onStatusChanged(String provider, int status, Bundle extras) { }

    @Override
    public void onProviderEnabled(String provider) { }

    @Override
    public void onProviderDisabled(String provider) { }
}
