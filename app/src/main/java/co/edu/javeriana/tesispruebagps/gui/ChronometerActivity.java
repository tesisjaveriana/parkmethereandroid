package co.edu.javeriana.tesispruebagps.gui;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Locale;

import co.edu.javeriana.tesispruebagps.R;
import co.edu.javeriana.tesispruebagps.controllers.BalanceMonitoringController;

public class ChronometerActivity extends AppCompatActivity {

    private BalanceMonitoringController balanceMonitoringController;
    private EditText priceEdit;
    private TextView arraiveText;
    private TextView priceText;
    private TextView balanceText;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_chronometer);

        balanceMonitoringController = new BalanceMonitoringController();

        Button startButon = (Button) findViewById(R.id.chronometerStartButton);
        Button stopButton = (Button) findViewById(R.id.chronometerStopButton);
        priceEdit = (EditText) findViewById(R.id.chronometerPriceEdit);
        arraiveText = (TextView) findViewById(R.id.chronometerArraiveHourText);
        priceText = (TextView) findViewById(R.id.chronometerPriceText);
        balanceText = (TextView) findViewById(R.id.chronometerBalanceText);

        if (balanceMonitoringController.isMonitoring()) {
            Calendar arraiveTime = balanceMonitoringController.getArraiveTime();
            float price = balanceMonitoringController.getPrice();
            double balance = balanceMonitoringController.getBalance();

            SimpleDateFormat df = new SimpleDateFormat("HH:mm", Locale.getDefault());
            String arraiveTimeStringFormat = df.format(arraiveTime.getTime());
            String priceStringFormat = "$ " + Float.toString(price);
            String balanceStringFormat = "$ " + Double.toString(balance);

            arraiveText.setText(arraiveTimeStringFormat);
            priceText.setText(priceStringFormat);
            balanceText.setText(balanceStringFormat);
        }

        startButon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String priceStringFormat = priceEdit.getText().toString();

                if (priceStringFormat.isEmpty()) {
                    Toast.makeText(ChronometerActivity.this, "Debe ingresar un precio", Toast.LENGTH_SHORT).show();
                } else {
                    Calendar arraiveTime;
                    float price = Float.parseFloat(priceStringFormat);

                    SimpleDateFormat df = new SimpleDateFormat("HH:mm", Locale.getDefault());
                    String arraiveTimeStringFormat;
                    priceStringFormat = "$ " + Float.toString(price);

                    balanceMonitoringController.startMonitoring(price);
                    arraiveTime = balanceMonitoringController.getArraiveTime();
                    arraiveTimeStringFormat = df.format(arraiveTime.getTime());

                    arraiveText.setText(arraiveTimeStringFormat);
                    priceText.setText(priceStringFormat);
                    Toast.makeText(ChronometerActivity.this, "Monitoreo iniciado", Toast.LENGTH_SHORT).show();
                }
            }
        });

        stopButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                balanceMonitoringController.stopMonitoring();
                String arraiveStringFormat = "00:00";
                arraiveText.setText(arraiveStringFormat);
                priceText.setText("$ 0.0");
                balanceText.setText("$ 0.0");
                Toast.makeText(ChronometerActivity.this, "Monitoreo detenido", Toast.LENGTH_SHORT).show();
            }
        });
    }
}
