package co.edu.javeriana.tesispruebagps.gps;

/**
 * Interfaz que debe implementar un observador para poder recibir un nuevo cambio d eposición.
 * Created by Santiago on 23/10/2015.
 */
public interface GPSObserver {
    /**
     * Acciones a ejecutar cuando hay un nuevo cambio de posición.
     * @param lat Nueva latitud.
     * @param lng Nueva longitud.
     */
    void notifyNewPosition( float lat, float lng );
}
