package co.edu.javeriana.tesispruebagps.gui;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.parse.ParseException;

import co.edu.javeriana.tesispruebagps.R;
import co.edu.javeriana.tesispruebagps.account.AccountController;
import co.edu.javeriana.tesispruebagps.universal.UniversalTestValues;

public class SignupActivity extends AppCompatActivity {
    private AccountController accountController;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_signup);

        Bundle extras = getIntent().getExtras();
        accountController = (AccountController) extras.getSerializable("account");

        Button signupButton = (Button) findViewById(R.id.signupSignupButton);

        signupButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                EditText usernameView = (EditText) findViewById(R.id.signupUsername);
                EditText passwordView = (EditText) findViewById(R.id.signupPassword);
                EditText confirmPasswordView = (EditText) findViewById(R.id.signupConfirmPassword);
                EditText firstNameView = (EditText) findViewById(R.id.signupFirstName);
                EditText lastNameView = (EditText) findViewById(R.id.signupLastName);
                EditText emailView = (EditText) findViewById(R.id.signupEmail);

                String username = usernameView.getText().toString();
                String password = passwordView.getText().toString();
                String confirmPassword = confirmPasswordView.getText().toString();
                String firstName = firstNameView.getText().toString();
                String lastName = lastNameView.getText().toString();
                String email = emailView.getText().toString();

                try {
                    if(accountController.signup(username, password, confirmPassword, firstName, lastName, email)) {
                        Bundle extras;
                        Intent mainIntent;

                        extras = new Bundle();
                        extras.putSerializable("account", accountController);
                        mainIntent = new Intent(SignupActivity.this, MapsActivity.class);
                        mainIntent.putExtras(extras);
                        startActivity(mainIntent);
                        LoginActivity.toFinish.finish();
                        finish();
                    } else {
                        Toast.makeText(SignupActivity.this, "Contraseñas incorrectas", Toast.LENGTH_SHORT).show();
                    }
                } catch (ParseException e) {
                    switch ((e.getCode())) {
                        case ParseException.ACCOUNT_ALREADY_LINKED:
                            Toast.makeText(SignupActivity.this, "Ya existe la cuenta", Toast.LENGTH_SHORT).show();
                            break;
                        case ParseException.CONNECTION_FAILED:
                            Toast.makeText(SignupActivity.this, "Fallo la conexión", Toast.LENGTH_SHORT).show();
                            break;
                        case ParseException.EMAIL_MISSING:
                            Toast.makeText(SignupActivity.this, "No s eingreso el correo", Toast.LENGTH_SHORT).show();
                            break;
                        case ParseException.EMAIL_TAKEN:
                            Toast.makeText(SignupActivity.this, "Ya existe el correo", Toast.LENGTH_SHORT).show();
                            break;
                        case ParseException.PASSWORD_MISSING:
                            Toast.makeText(SignupActivity.this, "Contraseña incorrecta", Toast.LENGTH_SHORT).show();
                            break;
                        case ParseException.USERNAME_TAKEN:
                            Toast.makeText(SignupActivity.this, "Ya existe el nombre de usuario", Toast.LENGTH_SHORT).show();
                            break;
                        default:
                            Toast.makeText(SignupActivity.this, "Ocurrio un error, por favor intentelo nuevamente", Toast.LENGTH_SHORT).show();
                    }
                    UniversalTestValues.debug(getClass(), e.toString());
                }
            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_signup, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
