package co.edu.javeriana.tesispruebagps.gui;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.TimePicker;
import android.widget.Toast;

import java.util.Calendar;

import co.edu.javeriana.tesispruebagps.R;
import co.edu.javeriana.tesispruebagps.account.AccountController;
import co.edu.javeriana.tesispruebagps.notification.StayingNotification;

public class StayingActivity extends AppCompatActivity {
    private AccountController accountController;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_staying);

        accountController = (AccountController) getIntent().getExtras().getSerializable("account");

        Button advertiseButton = (Button) findViewById(R.id.stayingAdvertiseButton);

        advertiseButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                TimePicker departureHourPicker = (TimePicker) findViewById(R.id.stayingTimePicker);
                Calendar departureHourCalendar = Calendar.getInstance();
                Toast advertiseToast;

                departureHourCalendar.set(Calendar.HOUR_OF_DAY, departureHourPicker.getCurrentHour());
                departureHourCalendar.set(Calendar.MINUTE, departureHourPicker.getCurrentMinute());

                Bundle extras = new Bundle();
                Intent inAdvertise = new Intent(StayingActivity.this, StayingNotification.class);
                extras.putString("command", "staying");
                extras.putSerializable("departureHour", departureHourCalendar);
                inAdvertise.putExtras(extras);
                startService(inAdvertise);
                advertiseToast = Toast.makeText(getApplicationContext(), "Estadía anunciada.",
                        Toast.LENGTH_LONG);
                advertiseToast.show();
            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_staying, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
