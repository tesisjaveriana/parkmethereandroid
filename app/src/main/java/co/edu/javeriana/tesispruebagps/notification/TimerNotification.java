package co.edu.javeriana.tesispruebagps.notification;

import android.app.IntentService;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.NotificationCompat;
import android.support.v4.app.TaskStackBuilder;

import co.edu.javeriana.tesispruebagps.R;
import co.edu.javeriana.tesispruebagps.gui.MyReservationsActivity;

/*
 * Created by Santiago on 03/11/2015.
 */
public class TimerNotification extends IntentService {
    /**
     * Creates an IntentService.  Invoked by your subclass's constructor.
     */
    public TimerNotification() {
        super("TimerNotification");
    }

    @Override
    protected void onHandleIntent(Intent intent) {
        Bundle extras = intent.getExtras();
        String notificationMessage = extras.getString("notificationMessage");

        NotificationCompat.Builder mBuilder = new NotificationCompat
                .Builder( getApplicationContext() )
                .setSmallIcon(android.R.drawable.ic_dialog_map)
                .setContentTitle("ParkApp")
                .setContentText(notificationMessage);
        Intent inContadorNotificacion = new Intent( TimerNotification.this, MyReservationsActivity.class );

        TaskStackBuilder stackBuilder = TaskStackBuilder.create(getApplicationContext());
        stackBuilder.addParentStack(MyReservationsActivity.class);
        stackBuilder.addNextIntent(inContadorNotificacion);
        PendingIntent resultPendingIntent = stackBuilder.getPendingIntent( 0, PendingIntent.FLAG_UPDATE_CURRENT );
        mBuilder.setContentIntent(resultPendingIntent);
        NotificationManager mNotificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);

        Notification theNotification = mBuilder.build();
        theNotification.defaults |= Notification.DEFAULT_SOUND;
        theNotification.defaults |= Notification.DEFAULT_VIBRATE;

        mNotificationManager.notify(1, theNotification);
    }
}
