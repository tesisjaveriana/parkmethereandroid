package co.edu.javeriana.tesispruebagps.directions;

import android.os.AsyncTask;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.URL;
import java.net.URLConnection;

import co.edu.javeriana.tesispruebagps.universal.UniversalTestValues;

/**
 * Hace una consulta a una URL dada y retorna la resupuesta. Esta definida para realizar consultas a
 * Google Directions API.
 * Created by Santiago on 12/10/2015.
 */
public class GetRequestTask extends AsyncTask<String, Void, String> {
    /**
     * Hace una consulta a una URL dada.
     * @param URLS URL a la cual se realizará la consulta.
     * @return Respuesta de la URL.
     */
    @Override
    protected String doInBackground(String... URLS) {
        String response = null;
        try {
            URL myURL = new URL(URLS[0]);
            URLConnection myURLConnection = myURL.openConnection();
            BufferedReader in = new BufferedReader(new InputStreamReader(
                    myURLConnection.getInputStream()));
            String inputLine;
            response = "";
            while ((inputLine = in.readLine()) != null) {
                response += inputLine;
            }
        } catch (IOException e) {
            UniversalTestValues.debug("Directions", e.toString());
        }
        return response;
    }
}
