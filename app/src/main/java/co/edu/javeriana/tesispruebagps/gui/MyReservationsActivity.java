package co.edu.javeriana.tesispruebagps.gui;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import com.parse.ParseException;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import co.edu.javeriana.tesispruebagps.R;
import co.edu.javeriana.tesispruebagps.account.AccountController;
import co.edu.javeriana.tesispruebagps.customviews.MyReservationListAdapter;
import co.edu.javeriana.tesispruebagps.dto.Reservation;
import co.edu.javeriana.tesispruebagps.controllers.MyReservationsController;
import co.edu.javeriana.tesispruebagps.universal.UniversalTestValues;

/**
 * Vista que expone la gestión de las reservas del usuario.
 */
public class MyReservationsActivity extends AppCompatActivity {
    /**
     * Controlador que gestiona las reservas del usuario.
     */
    private MyReservationsController myReservationsController;
    private AccountController accountController;

    private void showMyReservations() {
        // Consulta las reservas realizadas por el usuario.
        try {
            myReservationsController = new MyReservationsController(accountController.getUserID());
            ListView myReservationsList = (ListView) findViewById(R.id.myReservationsList);

            List<Reservation> myReservations = myReservationsController.getMyReservations();
            List<CharSequence> displayData = new ArrayList<>();
            List<String> resrvationsIDS = new ArrayList<>();

            if(myReservations.isEmpty()) {
                displayData.add("No ha solicitado reservas.");
                resrvationsIDS.add(null);
            } else {
                for (Reservation reservation : myReservations) {
                    int reservationState = reservation.getEstado();
                    String stateString = "Desconocido";
                    switch (reservationState) {
                        case 0:
                            stateString = "Por ocupar";
                            break;
                        case 1:
                            stateString = "Ocupando";
                            break;
                        case 2:
                            stateString = "Ocupado";
                            break;
                    }
                    String infoToDisplay = reservation.getNombreParqueadero()
                            + "\nEstado:" + stateString
                            + "\nHora llegada:" + new SimpleDateFormat("HH:mm", Locale.getDefault()).format(reservation.getHoraLlegada())
                            + "\nHora salida:" + new SimpleDateFormat("HH:mm", Locale.getDefault()).format(reservation.getHoraSalida());
                    displayData.add(infoToDisplay);
                    resrvationsIDS.add(reservation.getParseID());
                }
            }
            myReservationsList.setAdapter(new MyReservationListAdapter(this, displayData, resrvationsIDS));
        } catch (ParseException e) {
            UniversalTestValues.debug(getClass(), e.toString());
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_my_reservations);

        Bundle extras = getIntent().getExtras();
        accountController = new AccountController();
    }

    @Override
    protected void onResume() {
        super.onResume();
        showMyReservations();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_my_reservation, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
