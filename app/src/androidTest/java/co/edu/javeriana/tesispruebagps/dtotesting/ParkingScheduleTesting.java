package co.edu.javeriana.tesispruebagps.dtotesting;

import android.app.Activity;
import android.content.Context;
import android.test.ActivityUnitTestCase;
import android.test.suitebuilder.annotation.SmallTest;
import android.util.Pair;

import com.parse.ParseException;

import java.util.ArrayList;
import java.util.List;

import co.edu.javeriana.tesispruebagps.R;
import co.edu.javeriana.tesispruebagps.dto.ParkingInfo;
import co.edu.javeriana.tesispruebagps.dto.ParkingSchedule;
import co.edu.javeriana.tesispruebagps.gui.LoginActivity;

/**
 * Prueba ParkingSchedule
 * Created by Santiago on 23/11/2015.
 */
public class ParkingScheduleTesting extends ActivityUnitTestCase<LoginActivity> {
    public ParkingScheduleTesting() {
        super(LoginActivity.class);
    }

    @Override
    public void setUp() throws Exception {
        super.setUp();
        Context context = getInstrumentation().getTargetContext();
        context.setTheme(R.style.Theme_AppCompat);
        Activity loginActivity = launchActivity(context.getPackageName(),
                LoginActivity.class, null);
        getInstrumentation().waitForIdleSync();
        setActivity(loginActivity);
    }

    @SmallTest
    public void testGiraldo() throws ParseException {
        String userId = "sJgURFQiSC";
        String parkingId = "ipUYelcnqd";
        ParkingInfo parkingInfo = new ParkingInfo(parkingId);
        ParkingSchedule parkingSchedule = new ParkingSchedule(parkingInfo);
        String horaLlegada = "08:00";
        String horaSalida = "09:00";
        double saldo = 1800;

        assertEquals(parkingInfo, parkingSchedule.getSelectedParking());

        List<Pair<String,String>> expectedList = new ArrayList<>();
        // Depende de la hora actual
        /*
        expectedList.add(new Pair<>("07:00", "08:00"));
        expectedList.add(new Pair<>("08:00", "09:00"));
        expectedList.add(new Pair<>("09:00", "10:00"));
        expectedList.add(new Pair<>("10:00", "11:00"));
        expectedList.add(new Pair<>("11:00", "12:00"));
        expectedList.add(new Pair<>("12:00", "13:00"));
        expectedList.add(new Pair<>("13:00", "14:00"));
        expectedList.add(new Pair<>("14:00", "15:00"));
        expectedList.add(new Pair<>("15:00", "16:00"));
        expectedList.add(new Pair<>("16:00", "17:00"));
        expectedList.add(new Pair<>("17:00", "18:00"));*/
        List<Pair<String,String>> parkingScheduleList = parkingSchedule.getAvaliableSchedule();
        assertEquals(expectedList, parkingScheduleList);

        assertTrue(parkingSchedule.validateReservation(horaLlegada, horaSalida, userId));
        assertEquals(saldo, parkingSchedule.simulatePrice(horaLlegada, horaSalida));
    }
}
