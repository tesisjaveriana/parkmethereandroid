package co.edu.javeriana.tesispruebagps.dtotesting;

import android.app.Activity;
import android.content.Context;
import android.test.ActivityUnitTestCase;
import android.test.suitebuilder.annotation.SmallTest;

import com.google.android.gms.maps.model.LatLng;
import com.parse.ParseException;

import co.edu.javeriana.tesispruebagps.R;
import co.edu.javeriana.tesispruebagps.dto.ParkingInfo;
import co.edu.javeriana.tesispruebagps.gui.LoginActivity;

/**
 * Prueba ParkingInfo
 * Created by Santiago on 23/11/2015.
 */
public class ParkingInfoTesting extends ActivityUnitTestCase<LoginActivity> {
    public ParkingInfoTesting() {
        super(LoginActivity.class);
    }

    @Override
    public void setUp() throws Exception {
        super.setUp();
        Context context = getInstrumentation().getTargetContext();
        context.setTheme(R.style.Theme_AppCompat);
        Activity loginActivity = launchActivity(context.getPackageName(),
                LoginActivity.class, null);
        getInstrumentation().waitForIdleSync();
        setActivity(loginActivity);
    }

    @SmallTest
    public void testGiraldo() throws ParseException {
        String parkingId = "ipUYelcnqd";
        String nombre = "Giraldo";
        LatLng centroide = new LatLng(4.62663, -74.0649);
        double tarifa = 30;

        ParkingInfo parkingInfo = new ParkingInfo(parkingId);

        assertEquals(parkingId, parkingInfo.getObjectID());
        assertEquals(nombre, parkingInfo.getNombre());
        assertEquals(centroide, parkingInfo.getCentroid());
        assertEquals(tarifa, parkingInfo.getPrice());
        assertTrue(parkingInfo.isReservationAvaliable());
    }
}
